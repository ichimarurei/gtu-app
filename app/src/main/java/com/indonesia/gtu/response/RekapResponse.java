package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Rekap;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class RekapResponse {
    @SerializedName("data")
    @Expose
    private List<Rekap> rekapList;

    public RekapResponse() {
    }

    public List<Rekap> getRekapList() {
        return rekapList;
    }

    public void setRekapList(List<Rekap> rekapList) {
        this.rekapList = rekapList;
    }
}
