package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Presensi;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class PresensiResponse {
    @SerializedName("data")
    @Expose
    private List<Presensi> presensiList;

    public PresensiResponse() {
    }

    public List<Presensi> getPresensiList() {
        return presensiList;
    }

    public void setPresensiList(List<Presensi> presensiList) {
        this.presensiList = presensiList;
    }
}
