package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Proyek;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class ProyekResponse {
    @SerializedName("data")
    @Expose
    private List<Proyek> proyekList;

    public ProyekResponse() {
    }

    public List<Proyek> getProyekList() {
        return proyekList;
    }

    public void setProyekList(List<Proyek> proyekList) {
        this.proyekList = proyekList;
    }
}
