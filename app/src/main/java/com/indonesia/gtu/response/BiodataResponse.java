package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class BiodataResponse {
    @SerializedName("data")
    @Expose
    private Biodata biodata;

    public BiodataResponse() {
    }

    public Biodata getBiodata() {
        return biodata;
    }

    public void setBiodata(Biodata biodata) {
        this.biodata = biodata;
    }

    public class Biodata {
        String key;
        String kode;
        String id;
        String ktp;
        String npwp;
        String bpjs;
        String nama;
        String kelamin;
        String agama;
        String pendidikan;
        String rekening;
        String alamat;
        String telepon;
        String email;
        String nikah;
        String anak;

        @SerializedName("tempat_lahir")
        @Expose
        String tempatLahir;

        @SerializedName("tanggal_lahir")
        @Expose
        String tanggalLahir;

        public Biodata() {
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getKode() {
            return kode;
        }

        public void setKode(String kode) {
            this.kode = kode;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getKtp() {
            return ktp;
        }

        public void setKtp(String ktp) {
            this.ktp = ktp;
        }

        public String getNpwp() {
            return npwp;
        }

        public void setNpwp(String npwp) {
            this.npwp = npwp;
        }

        public String getBpjs() {
            return bpjs;
        }

        public void setBpjs(String bpjs) {
            this.bpjs = bpjs;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getKelamin() {
            return kelamin;
        }

        public void setKelamin(String kelamin) {
            this.kelamin = kelamin;
        }

        public String getAgama() {
            return agama;
        }

        public void setAgama(String agama) {
            this.agama = agama;
        }

        public String getPendidikan() {
            return pendidikan;
        }

        public void setPendidikan(String pendidikan) {
            this.pendidikan = pendidikan;
        }

        public String getRekening() {
            return rekening;
        }

        public void setRekening(String rekening) {
            this.rekening = rekening;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getTelepon() {
            return telepon;
        }

        public void setTelepon(String telepon) {
            this.telepon = telepon;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getNikah() {
            return nikah;
        }

        public void setNikah(String nikah) {
            this.nikah = nikah;
        }

        public String getAnak() {
            return anak;
        }

        public void setAnak(String anak) {
            this.anak = anak;
        }

        public String getTempatLahir() {
            return tempatLahir;
        }

        public void setTempatLahir(String tempatLahir) {
            this.tempatLahir = tempatLahir;
        }

        public String getTanggalLahir() {
            return tanggalLahir;
        }

        public void setTanggalLahir(String tanggalLahir) {
            this.tanggalLahir = tanggalLahir;
        }
    }
}
