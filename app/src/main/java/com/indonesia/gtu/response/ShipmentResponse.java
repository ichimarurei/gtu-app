package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Shipment;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class ShipmentResponse {
    @SerializedName("data")
    @Expose
    private List<Shipment> shipmentList;

    public ShipmentResponse() {
    }

    public List<Shipment> getShipmentList() {
        return shipmentList;
    }

    public void setShipmentList(List<Shipment> shipmentList) {
        this.shipmentList = shipmentList;
    }
}
