package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Lembur;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class LemburResponse {
    @SerializedName("data")
    @Expose
    private List<Lembur> lemburList;

    public LemburResponse() {
    }

    public List<Lembur> getLemburList() {
        return lemburList;
    }

    public void setLemburList(List<Lembur> lemburList) {
        this.lemburList = lemburList;
    }
}
