package com.indonesia.gtu.response;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class BaseResponse {
    private String status;

    public BaseResponse() {//
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
