package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Arsip;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class ArsipResponse {
    @SerializedName("data")
    @Expose
    private List<Arsip> rekapDetailList;

    public ArsipResponse() {
    }


    public List<Arsip> getRekapDetailList() {
        return rekapDetailList;
    }

    public void setRekapDetailList(List<Arsip> rekapDetailList) {
        this.rekapDetailList = rekapDetailList;
    }
}
