package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class ProyekDetailResponse {
    @SerializedName("data")
    @Expose
    private ProyekDetail proyekDetail;

    public ProyekDetailResponse() {
    }

    public ProyekDetail getProyekDetail() {
        return proyekDetail;
    }

    public void setProyekDetail(ProyekDetail proyekDetail) {
        this.proyekDetail = proyekDetail;
    }

    public class ProyekDetail {
        @SerializedName("key")
        @Expose
        private String key;

        @SerializedName("kode")
        @Expose
        private String kode;

        @SerializedName("proyek")
        @Expose
        private String proyek;

        @SerializedName("alamat")
        @Expose
        private String alamat;

        @SerializedName("tipe")
        @Expose
        private String tipe;

        @SerializedName("lokasi")
        @Expose
        private String lokasi;

        @SerializedName("kendaraan")
        @Expose
        private String kendaraan;

        @SerializedName("invoice_inap")
        @Expose
        private String invoiceInap;

        @SerializedName("invoice_skr")
        @Expose
        private String invoiceSkr;

        @SerializedName("invoice_bpjs")
        @Expose
        private String invoiceBpjs;

        @SerializedName("invoice_ritase")
        @Expose
        private String invoiceRitase;

        @SerializedName("invoice_pengiriman")
        @Expose
        private String invoicePengiriman;

        @SerializedName("invoice_fee_korlap")
        @Expose
        private String invoiceFeeKorlap;

        @SerializedName("invoice_fee_driver")
        @Expose
        private String invoiceFeeDriver;

        @SerializedName("invoice_fee_helper")
        @Expose
        private String invoiceFeeHelper;

        @SerializedName("invoice_pph")
        @Expose
        private String invoicePph;

        @SerializedName("invoice_ppn")
        @Expose
        private String invoicePpn;

        @SerializedName("invoice_umk")
        @Expose
        private String invoiceUmk;

        @SerializedName("invoice_thr")
        @Expose
        private String invoiceThr;

        @SerializedName("invoice_supervisi")
        @Expose
        private String invoiceSupervisi;

        @SerializedName("invoice_seragam")
        @Expose
        private String invoiceSeragam;

        @SerializedName("gaji_korlap")
        @Expose
        private String gajiKorlap;

        @SerializedName("makan_korlap")
        @Expose
        private String makanKorlap;

        @SerializedName("gaji_driver")
        @Expose
        private String gajiDriver;

        @SerializedName("makan_driver")
        @Expose
        private String makanDriver;

        @SerializedName("gaji_helper")
        @Expose
        private String gajiHelper;

        @SerializedName("makan_helper")
        @Expose
        private String makanHelper;

        @SerializedName("gaji_admin")
        @Expose
        private String gajiAdmin;

        @SerializedName("makan_admin")
        @Expose
        private String makanAdmin;

        @SerializedName("gaji_staf")
        @Expose
        private String gajiStaf;

        @SerializedName("makan_staf")
        @Expose
        private String makanStaf;

        @SerializedName("lembur_driver")
        @Expose
        private String lemburDriver;

        @SerializedName("lembur_helper")
        @Expose
        private String lemburHelper;

        @SerializedName("lembur_korlap")
        @Expose
        private String lemburKorlap;

        @SerializedName("terpakai")
        @Expose
        private String terpakai;

        @SerializedName("tipeText")
        @Expose
        private String tipeText;

        @SerializedName("lokasiText")
        @Expose
        private String lokasiText;

        @SerializedName("kendaraanText")
        @Expose
        private String kendaraanText;

        @SerializedName("gaji_lembur_driver")
        @Expose
        private String lemburanDriver;

        @SerializedName("gaji_lembur_helper")
        @Expose
        private String lemburanHelper;

        @SerializedName("gaji_lembur_korlap")
        @Expose
        private String lemburanKorlap;

        @SerializedName("gaji_inap")
        @Expose
        private String uangInap;

        @SerializedName("gaji_skr")
        @Expose
        private String uangSKR;

        @SerializedName("gaji_ritase")
        @Expose
        private String uangRitase;

        @SerializedName("gaji_insentif")
        @Expose
        private String uangUpah;

        public String getLemburanDriver() {
            return lemburanDriver;
        }

        public void setLemburanDriver(String lemburanDriver) {
            this.lemburanDriver = lemburanDriver;
        }

        public String getLemburanHelper() {
            return lemburanHelper;
        }

        public void setLemburanHelper(String lemburanHelper) {
            this.lemburanHelper = lemburanHelper;
        }

        public String getLemburanKorlap() {
            return lemburanKorlap;
        }

        public void setLemburanKorlap(String lemburanKorlap) {
            this.lemburanKorlap = lemburanKorlap;
        }

        public String getUangInap() {
            return uangInap;
        }

        public void setUangInap(String uangInap) {
            this.uangInap = uangInap;
        }

        public String getUangSKR() {
            return uangSKR;
        }

        public void setUangSKR(String uangSKR) {
            this.uangSKR = uangSKR;
        }

        public String getUangRitase() {
            return uangRitase;
        }

        public void setUangRitase(String uangRitase) {
            this.uangRitase = uangRitase;
        }

        public String getUangUpah() {
            return uangUpah;
        }

        public void setUangUpah(String uangUpah) {
            this.uangUpah = uangUpah;
        }

        public String getLemburKorlap() {
            return lemburKorlap;
        }

        public void setLemburKorlap(String lemburKorlap) {
            this.lemburKorlap = lemburKorlap;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getKode() {
            return kode;
        }

        public void setKode(String kode) {
            this.kode = kode;
        }

        public String getProyek() {
            return proyek;
        }

        public void setProyek(String proyek) {
            this.proyek = proyek;
        }

        public String getAlamat() {
            return alamat;
        }

        public void setAlamat(String alamat) {
            this.alamat = alamat;
        }

        public String getTipe() {
            return tipe;
        }

        public void setTipe(String tipe) {
            this.tipe = tipe;
        }

        public String getLokasi() {
            return lokasi;
        }

        public void setLokasi(String lokasi) {
            this.lokasi = lokasi;
        }

        public String getKendaraan() {
            return kendaraan;
        }

        public void setKendaraan(String kendaraan) {
            this.kendaraan = kendaraan;
        }

        public String getInvoiceInap() {
            return invoiceInap;
        }

        public void setInvoiceInap(String invoiceInap) {
            this.invoiceInap = invoiceInap;
        }

        public String getInvoiceSkr() {
            return invoiceSkr;
        }

        public void setInvoiceSkr(String invoiceSkr) {
            this.invoiceSkr = invoiceSkr;
        }

        public String getInvoiceBpjs() {
            return invoiceBpjs;
        }

        public void setInvoiceBpjs(String invoiceBpjs) {
            this.invoiceBpjs = invoiceBpjs;
        }

        public String getInvoiceRitase() {
            return invoiceRitase;
        }

        public void setInvoiceRitase(String invoiceRitase) {
            this.invoiceRitase = invoiceRitase;
        }

        public String getInvoicePengiriman() {
            return invoicePengiriman;
        }

        public void setInvoicePengiriman(String invoicePengiriman) {
            this.invoicePengiriman = invoicePengiriman;
        }

        public String getInvoiceFeeKorlap() {
            return invoiceFeeKorlap;
        }

        public void setInvoiceFeeKorlap(String invoiceFeeKorlap) {
            this.invoiceFeeKorlap = invoiceFeeKorlap;
        }

        public String getInvoiceFeeDriver() {
            return invoiceFeeDriver;
        }

        public void setInvoiceFeeDriver(String invoiceFeeDriver) {
            this.invoiceFeeDriver = invoiceFeeDriver;
        }

        public String getInvoiceFeeHelper() {
            return invoiceFeeHelper;
        }

        public void setInvoiceFeeHelper(String invoiceFeeHelper) {
            this.invoiceFeeHelper = invoiceFeeHelper;
        }

        public String getInvoicePph() {
            return invoicePph;
        }

        public void setInvoicePph(String invoicePph) {
            this.invoicePph = invoicePph;
        }

        public String getInvoicePpn() {
            return invoicePpn;
        }

        public void setInvoicePpn(String invoicePpn) {
            this.invoicePpn = invoicePpn;
        }

        public String getInvoiceUmk() {
            return invoiceUmk;
        }

        public void setInvoiceUmk(String invoiceUmk) {
            this.invoiceUmk = invoiceUmk;
        }

        public String getInvoiceThr() {
            return invoiceThr;
        }

        public void setInvoiceThr(String invoiceThr) {
            this.invoiceThr = invoiceThr;
        }

        public String getInvoiceSupervisi() {
            return invoiceSupervisi;
        }

        public void setInvoiceSupervisi(String invoiceSupervisi) {
            this.invoiceSupervisi = invoiceSupervisi;
        }

        public String getInvoiceSeragam() {
            return invoiceSeragam;
        }

        public void setInvoiceSeragam(String invoiceSeragam) {
            this.invoiceSeragam = invoiceSeragam;
        }

        public String getGajiKorlap() {
            return gajiKorlap;
        }

        public void setGajiKorlap(String gajiKorlap) {
            this.gajiKorlap = gajiKorlap;
        }

        public String getMakanKorlap() {
            return makanKorlap;
        }

        public void setMakanKorlap(String makanKorlap) {
            this.makanKorlap = makanKorlap;
        }

        public String getGajiDriver() {
            return gajiDriver;
        }

        public void setGajiDriver(String gajiDriver) {
            this.gajiDriver = gajiDriver;
        }

        public String getMakanDriver() {
            return makanDriver;
        }

        public void setMakanDriver(String makanDriver) {
            this.makanDriver = makanDriver;
        }

        public String getGajiHelper() {
            return gajiHelper;
        }

        public void setGajiHelper(String gajiHelper) {
            this.gajiHelper = gajiHelper;
        }

        public String getMakanHelper() {
            return makanHelper;
        }

        public void setMakanHelper(String makanHelper) {
            this.makanHelper = makanHelper;
        }

        public String getGajiAdmin() {
            return gajiAdmin;
        }

        public void setGajiAdmin(String gajiAdmin) {
            this.gajiAdmin = gajiAdmin;
        }

        public String getMakanAdmin() {
            return makanAdmin;
        }

        public void setMakanAdmin(String makanAdmin) {
            this.makanAdmin = makanAdmin;
        }

        public String getGajiStaf() {
            return gajiStaf;
        }

        public void setGajiStaf(String gajiStaf) {
            this.gajiStaf = gajiStaf;
        }

        public String getMakanStaf() {
            return makanStaf;
        }

        public void setMakanStaf(String makanStaf) {
            this.makanStaf = makanStaf;
        }

        public String getLemburDriver() {
            return lemburDriver;
        }

        public void setLemburDriver(String lemburDriver) {
            this.lemburDriver = lemburDriver;
        }

        public String getLemburHelper() {
            return lemburHelper;
        }

        public void setLemburHelper(String lemburHelper) {
            this.lemburHelper = lemburHelper;
        }

        public String getTerpakai() {
            return terpakai;
        }

        public void setTerpakai(String terpakai) {
            this.terpakai = terpakai;
        }

        public String getTipeText() {
            return tipeText;
        }

        public void setTipeText(String tipeText) {
            this.tipeText = tipeText;
        }

        public String getLokasiText() {
            return lokasiText;
        }

        public void setLokasiText(String lokasiText) {
            this.lokasiText = lokasiText;
        }

        public String getKendaraanText() {
            return kendaraanText;
        }

        public void setKendaraanText(String kendaraanText) {
            this.kendaraanText = kendaraanText;
        }
    }
}
