package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Karyawan;
import com.indonesia.gtu.model.Proyek;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class KaryawanResponse {
    @SerializedName("data")
    @Expose
    private List<Karyawan> karyawanList;

    public KaryawanResponse() {
    }

    public List<Karyawan> getKaryawanList() {
        return karyawanList;
    }

    public void setKaryawanList(List<Karyawan> karyawanList) {
        this.karyawanList = karyawanList;
    }
}
