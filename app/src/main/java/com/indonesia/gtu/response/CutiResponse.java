package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Cuti;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class CutiResponse {
    @SerializedName("data")
    @Expose
    private List<Cuti> cutiList;

    public CutiResponse() {
    }

    public List<Cuti> getCutiList() {
        return cutiList;
    }

    public void setCutiList(List<Cuti> cutiList) {
        this.cutiList = cutiList;
    }
}
