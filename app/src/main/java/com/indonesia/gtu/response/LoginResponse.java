package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class LoginResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private User user;

    public LoginResponse() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public class User {
        @SerializedName("_akun")
        @Expose
        String akun;

        @SerializedName("_bio")
        @Expose
        String bio;

        @SerializedName("_id")
        @Expose
        String id;

        @SerializedName("_otoritas")
        @Expose
        String otoritas;

        @SerializedName("_proyek")
        @Expose
        String proyek;

        public User() {
        }

        public String getAkun() {
            return akun;
        }

        public void setAkun(String akun) {
            this.akun = akun;
        }

        public String getBio() {
            return bio;
        }

        public void setBio(String bio) {
            this.bio = bio;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOtoritas() {
            return otoritas;
        }

        public void setOtoritas(String otoritas) {
            this.otoritas = otoritas;
        }

        public String getProyek() {
            return proyek;
        }

        public void setProyek(String proyek) {
            this.proyek = proyek;
        }
    }
}
