package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class LiveLocationResponse {
    @SerializedName("data")
    @Expose
    private LiveLocation liveLocation;

    public LiveLocationResponse() {
    }

    public LiveLocation getLiveLocation() {
        return liveLocation;
    }

    public void setLiveLocation(LiveLocation liveLocation) {
        this.liveLocation = liveLocation;
    }

    public class LiveLocation {
        @SerializedName("long")
        @Expose
        String longitude;

        @SerializedName("lat")
        @Expose
        String latitude;

        String waktu;

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getWaktu() {
            return waktu;
        }

        public void setWaktu(String waktu) {
            this.waktu = waktu;
        }
    }
}
