package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class KarirResponse {
    @SerializedName("data")
    @Expose
    private Karir karir;

    public Karir getKarir() {
        return karir;
    }

    public void setKarir(Karir karir) {
        this.karir = karir;
    }

    public KarirResponse() {
    }

    public class Karir {
        String nik;
        String nama;
        String jabatan;
        String hadir;
        String izin;
        String alfa;
        String sakit;
        String lembur;

        @SerializedName("hadir_sekarang")
        @Expose
        HadirSekarang hadirSekarang;

        @SerializedName("pulang_sekarang")
        @Expose
        PulangSekarang pulangSekarang;


        @SerializedName("lembur_sekarang")
        @Expose
        LemburSekarang lemburSekarang;

        Statistik statistik;

        @SerializedName("sp")
        @Expose
        List<SuratPeringartan> suratPeringartanList;

        public String getNik() {
            return nik;
        }

        public void setNik(String nik) {
            this.nik = nik;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getJabatan() {
            return jabatan;
        }

        public void setJabatan(String jabatan) {
            this.jabatan = jabatan;
        }

        public String getHadir() {
            return hadir;
        }

        public void setHadir(String hadir) {
            this.hadir = hadir;
        }

        public String getIzin() {
            return izin;
        }

        public void setIzin(String izin) {
            this.izin = izin;
        }

        public String getAlfa() {
            return alfa;
        }

        public void setAlfa(String alfa) {
            this.alfa = alfa;
        }

        public String getSakit() {
            return sakit;
        }

        public void setSakit(String sakit) {
            this.sakit = sakit;
        }

        public String getLembur() {
            return lembur;
        }

        public void setLembur(String lembur) {
            this.lembur = lembur;
        }

        public HadirSekarang getHadirSekarang() {
            return hadirSekarang;
        }

        public void setHadirSekarang(HadirSekarang hadirSekarang) {
            this.hadirSekarang = hadirSekarang;
        }

        public PulangSekarang getPulangSekarang() {
            return pulangSekarang;
        }

        public void setPulangSekarang(PulangSekarang pulangSekarang) {
            this.pulangSekarang = pulangSekarang;
        }

        public LemburSekarang getLemburSekarang() {
            return lemburSekarang;
        }

        public void setLemburSekarang(LemburSekarang lemburSekarang) {
            this.lemburSekarang = lemburSekarang;
        }

        public Statistik getStatistik() {
            return statistik;
        }

        public void setStatistik(Statistik statistik) {
            this.statistik = statistik;
        }

        public List<SuratPeringartan> getSuratPeringartanList() {
            return suratPeringartanList;
        }

        public void setSuratPeringartanList(List<SuratPeringartan> suratPeringartanList) {
            this.suratPeringartanList = suratPeringartanList;
        }
    }

    public class HadirSekarang {
        String warna;
        String jam;
        String info;

        public String getWarna() {
            return warna;
        }

        public void setWarna(String warna) {
            this.warna = warna;
        }

        public String getJam() {
            return jam;
        }

        public void setJam(String jam) {
            this.jam = jam;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }
    }

    public class PulangSekarang {
        String warna;
        String jam;
        String info;

        public String getWarna() {
            return warna;
        }

        public void setWarna(String warna) {
            this.warna = warna;
        }

        public String getJam() {
            return jam;
        }

        public void setJam(String jam) {
            this.jam = jam;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }
    }

    public class LemburSekarang {
        String warna;
        String info;

        public String getWarna() {
            return warna;
        }

        public void setWarna(String warna) {
            this.warna = warna;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }
    }

    public class Statistik {
        String hadir;
        String izin;
        String alfa;
        String sakit;

        public String getHadir() {
            return hadir;
        }

        public void setHadir(String hadir) {
            this.hadir = hadir;
        }

        public String getIzin() {
            return izin;
        }

        public void setIzin(String izin) {
            this.izin = izin;
        }

        public String getAlfa() {
            return alfa;
        }

        public void setAlfa(String alfa) {
            this.alfa = alfa;
        }

        public String getSakit() {
            return sakit;
        }

        public void setSakit(String sakit) {
            this.sakit = sakit;
        }
    }

    public class SuratPeringartan {
        String tanggal;
        String nomor;
        String ke;

        public String getTanggal() {
            return tanggal;
        }

        public void setTanggal(String tanggal) {
            this.tanggal = tanggal;
        }

        public String getNomor() {
            return nomor;
        }

        public void setNomor(String nomor) {
            this.nomor = nomor;
        }

        public String getKe() {
            return ke;
        }

        public void setKe(String ke) {
            this.ke = ke;
        }
    }
}
