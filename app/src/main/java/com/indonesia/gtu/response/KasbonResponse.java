package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Klaim;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class KasbonResponse {
    @SerializedName("data")
    @Expose
    private List<Klaim> cutiList;

    public KasbonResponse() {
    }

    public List<Klaim> getKlaimList() {
        return cutiList;
    }

    public void setKlaimList(List<Klaim> cutiList) {
        this.cutiList = cutiList;
    }
}
