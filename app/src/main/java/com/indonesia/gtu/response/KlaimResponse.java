package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Klaim;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class KlaimResponse {
    @SerializedName("data")
    @Expose
    private List<Klaim> klaimList;

    public KlaimResponse() {
    }

    public List<Klaim> getKlaimList() {
        return klaimList;
    }

    public void setKlaimList(List<Klaim> klaimList) {
        this.klaimList = klaimList;
    }
}
