package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indonesia.gtu.model.Pulang;

import java.util.List;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class PulangResponse {
    @SerializedName("data")
    @Expose
    private List<Pulang> pulangList;

    public PulangResponse() {
    }

    public List<Pulang> getPulangList() {
        return pulangList;
    }

    public void setPulangList(List<Pulang> pulangList) {
        this.pulangList = pulangList;
    }
}
