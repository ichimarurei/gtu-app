package com.indonesia.gtu.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by doni.wahyu on 11/3/2018.
 */

public class RekapDetailResponse {
    @SerializedName("data")
    @Expose
    private RekapDetail rekapDetail;

    public RekapDetailResponse() {
    }

    public RekapDetail getRekapDetail() {
        return rekapDetail;
    }

    public void setRekapDetail(RekapDetail rekapDetail) {
        this.rekapDetail = rekapDetail;
    }

    public class RekapDetail {
        private String key;
        private String kode;
        private String dari;
        private String hingga;
        private String proyek;
        private String status;
        private String waktu;
        private String jenis;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getKode() {
            return kode;
        }

        public void setKode(String kode) {
            this.kode = kode;
        }

        public String getDari() {
            return dari;
        }

        public void setDari(String dari) {
            this.dari = dari;
        }

        public String getHingga() {
            return hingga;
        }

        public void setHingga(String hingga) {
            this.hingga = hingga;
        }

        public String getProyek() {
            return proyek;
        }

        public void setProyek(String proyek) {
            this.proyek = proyek;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getWaktu() {
            return waktu;
        }

        public void setWaktu(String waktu) {
            this.waktu = waktu;
        }

        public String getJenis() {
            return jenis;
        }

        public void setJenis(String jenis) {
            this.jenis = jenis;
        }
    }
}
