package com.indonesia.gtu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.model.Menu;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by DONI on 8/11/2018.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    private List<Menu> dataList;
    private Context context;
    final ClickListener clickListener;

    public MenuAdapter(List<Menu> dataList, Context context, ClickListener clickListener) {
        this.dataList = dataList;
        this.context = context;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_menu, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.menuNameTextView.setText(dataList.get(position).getMenuName());

        Picasso.with(context)
                .load(dataList.get(position).getResourceId())
                .fit().centerCrop()
                .into(holder.menuImageView, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });

        if (dataList.get(position).isEnable()) {
            holder.menuImageView.setAlpha(255);
            holder.menuNameTextView.setTextColor(context.getResources().getColor(R.color.textColorPrimary));
        } else {
            holder.menuImageView.setAlpha(50);
            holder.menuNameTextView.setTextColor(context.getResources().getColor(R.color.greyLight));
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        WeakReference<ClickListener> clickListenerRef;

        @BindView(R.id.menuImageView)
        ImageView menuImageView;

        @BindView(R.id.menuNameTextView)
        TextView menuNameTextView;

        ViewHolder(final View itemView) {
            super(itemView);
            clickListenerRef = new WeakReference<>(clickListener);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListenerRef.get().onItemClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListenerRef.get().onItemLongClick(v, getAdapterPosition());
            return true;
        }
    }
}