package com.indonesia.gtu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.model.Karyawan;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by DONI on 8/11/2018.
 */

public class KaryawanAdapter extends RecyclerView.Adapter<KaryawanAdapter.ViewHolder> {

    private List<Karyawan> dataList;
    private Context context;
    final ClickListener clickListener;

    public KaryawanAdapter(List<Karyawan> dataList, Context context, ClickListener clickListener) {
        this.dataList = dataList;
        this.context = context;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_karyawan, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.namaKaryawanTextView.setText(dataList.get(position).getNama());
        holder.proyekTextView.setText(dataList.get(position).getProyek());
        holder.phoneTextView.setText(Utility.stripHtml(dataList.get(position).getTelepon()));
        holder.kontrakTextView.setText(dataList.get(position).getKontrak());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        WeakReference<ClickListener> clickListenerRef;

        @BindView(R.id.namaKaryawanTextView)
        TextView namaKaryawanTextView;

        @BindView(R.id.proyekTextView)
        TextView proyekTextView;

        @BindView(R.id.phoneTextView)
        TextView phoneTextView;

        @BindView(R.id.kontrakTextView)
        TextView kontrakTextView;

        ViewHolder(final View itemView) {
            super(itemView);
            clickListenerRef = new WeakReference<>(clickListener);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListenerRef.get().onItemClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListenerRef.get().onItemLongClick(v, getAdapterPosition());
            return true;
        }
    }
}