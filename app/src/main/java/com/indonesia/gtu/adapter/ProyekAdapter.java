package com.indonesia.gtu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.model.Proyek;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by DONI on 8/11/2018.
 */

public class ProyekAdapter extends RecyclerView.Adapter<ProyekAdapter.ViewHolder> {

    private List<Proyek> dataList;
    private Context context;
    final ClickListener clickListener;

    public ProyekAdapter(List<Proyek> dataList, Context context, ClickListener clickListener) {
        this.dataList = dataList;
        this.context = context;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_proyek, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.proyekTextView.setText(dataList.get(position).getProyek());
        holder.tipeTextView.setText(dataList.get(position).getTipe());
        holder.locationTextView.setText(dataList.get(position).getLokasi());
        holder.kendaraanTextView.setText(dataList.get(position).getKendaraan());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        WeakReference<ClickListener> clickListenerRef;

        @BindView(R.id.locationImageView)
        ImageView locationImageView;

        @BindView(R.id.kendaraanImageView)
        ImageView kendaraanImageView;

        @BindView(R.id.proyekTextView)
        TextView proyekTextView;

        @BindView(R.id.tipeTextView)
        TextView tipeTextView;

        @BindView(R.id.locationTextView)
        TextView locationTextView;

        @BindView(R.id.kendaraanTextView)
        TextView kendaraanTextView;

        ViewHolder(final View itemView) {
            super(itemView);
            clickListenerRef = new WeakReference<>(clickListener);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListenerRef.get().onItemClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            clickListenerRef.get().onItemLongClick(v, getAdapterPosition());
            return true;
        }
    }
}