package com.indonesia.gtu.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.model.SuratPeringatan;

import java.lang.ref.WeakReference;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SuratPeringatanAdapter extends RecyclerView.Adapter<SuratPeringatanAdapter.ViewHolder> {
    private final ClickListener clickListener;
    private List<SuratPeringatan> dataList;
    private Context context;

    public SuratPeringatanAdapter(List<SuratPeringatan> dataList, Context context, ClickListener clickListener) {
        this.dataList = dataList;
        this.context = context;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_surat_peringatan, parent, false);

        return new SuratPeringatanAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.namaKaryawanTextView.setText(dataList.get(position).getBiodata());
        holder.proyekTextView.setText(dataList.get(position).getProyek());
        holder.waktuTextView.setText(Utility.stripHtml(dataList.get(position).getTanggal()));
        holder.nomorTextView.setText(dataList.get(position).getNomor());
        holder.keTextView.setText(dataList.get(position).getKe());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        @BindView(R.id.namaKaryawanTextView)
        TextView namaKaryawanTextView;

        @BindView(R.id.proyekTextView)
        TextView proyekTextView;

        @BindView(R.id.waktuTextView)
        TextView waktuTextView;

        @BindView(R.id.nomorTextView)
        TextView nomorTextView;

        @BindView(R.id.keTextView)
        TextView keTextView;

        private WeakReference<ClickListener> clickListenerRef;

        public ViewHolder(View itemView) {
            super(itemView);
            clickListenerRef = new WeakReference<>(clickListener);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListenerRef.get().onItemClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            clickListenerRef.get().onItemLongClick(view, getAdapterPosition());

            return true;
        }
    }
}
