package com.indonesia.gtu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.indonesia.gtu.R;
import com.indonesia.gtu.adapter.ProyekAdapter;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.model.Proyek;
import com.indonesia.gtu.response.ProyekResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProyekActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    String requestType = "";

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    SearchView searchView;
    List<Proyek> proyekList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyek);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Data Proyek");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (getIntent().hasExtra(Constants.REQUEST_TYPE)) {
            requestType = getIntent().getStringExtra(Constants.REQUEST_TYPE);
        }

        getProyekData();
    }

    @Override
    public void onRefresh() {
        searchView.setQuery("", false);
        searchView.requestFocus();
        getProyekData();
    }

    void getProyekData() {
        proyekList = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        recyclerView.setAdapter(null);
        Call<ProyekResponse> service = api.getProyekList();
        service.enqueue(new Callback<ProyekResponse>() {
            @Override
            public void onResponse(Call<ProyekResponse> call, Response<ProyekResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                ProyekResponse proyekResponse = response.body();
                proyekList = proyekResponse.getProyekList();
                setAdapterDataRecycleView("");
            }

            @Override
            public void onFailure(Call<ProyekResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                call.cancel();
                toast(Constants.SERVER_RESPONSE_ERROR);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setAdapterDataRecycleView(newText);
                return true;
            }
        });

        return true;
    }

    private void setAdapterDataRecycleView(String keyword) {
        final List<Proyek> filterList = new ArrayList<>();

        if (keyword.length() > 0) {
            for (Proyek proyek : proyekList) {
                if (proyek.getProyek().toLowerCase().contains(keyword)) {
                    filterList.add(proyek);
                }
            }
        } else {
            filterList.addAll(proyekList);
        }

        recyclerView.setAdapter(new ProyekAdapter(filterList, getApplicationContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (requestType.equalsIgnoreCase(Constants.REQUEST_INTENT_RESULT)) {
                    Intent intent = new Intent();
//                    intent.putExtra("id", Utility.getCodeFromAksi(filterList.get(position).getAksi()));
                    intent.putExtra("id", filterList.get(position).getKode());
                    intent.putExtra("text", filterList.get(position).getProyek());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), ProyekDetailActivity.class)
                            .putExtra("proyekKode", filterList.get(position).getKode()));
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
