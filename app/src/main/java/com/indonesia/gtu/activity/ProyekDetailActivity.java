package com.indonesia.gtu.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.response.ProyekDetailResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProyekDetailActivity extends BaseActivity {

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.alamatTextView)
    TextView alamatTextView;

    @BindView(R.id.lokasiTextView)
    TextView lokasiTextView;

    @BindView(R.id.jenisTextView)
    TextView jenisTextView;

    @BindView(R.id.kendaraanTextView)
    TextView kendaraanTextView;

    @BindView(R.id.muatInapTextView)
    TextView muatInapTextView;

    @BindView(R.id.skrTextView)
    TextView skrTextView;

    @BindView(R.id.bpjsTextView)
    TextView bpjsTextView;

    @BindView(R.id.ritaseTextView)
    TextView ritaseTextView;

    @BindView(R.id.pph23TextView)
    TextView pph23TextView;

    @BindView(R.id.ppnTextView)
    TextView ppnTextView;

    @BindView(R.id.insentifPengirimanTextView)
    TextView insentifPengirimanTextView;

    @BindView(R.id.supervisiTextView)
    TextView supervisiTextView;

    @BindView(R.id.umkTextView)
    TextView umkTextView;

    @BindView(R.id.seragamTextView)
    TextView seragamTextView;

    @BindView(R.id.thrTextView)
    TextView thrTextView;

    @BindView(R.id.feeKorlapTextView)
    TextView feeKorlapTextView;

    @BindView(R.id.feeDriverTextView)
    TextView feeDriverTextView;

    @BindView(R.id.feeHelperTextView)
    TextView feeHelperTextView;

    @BindView(R.id.gajiDriverTextView)
    TextView gajiDriverTextView;

    @BindView(R.id.uangMakanDriverTextView)
    TextView uangMakanDriverTextView;

    @BindView(R.id.gajiHelperTextView)
    TextView gajiHelperTextView;

    @BindView(R.id.uangMakanHelperTextView)
    TextView uangMakanHelperTextView;

    @BindView(R.id.gajiKorlapTextView)
    TextView gajiKorlapTextView;

    @BindView(R.id.uangMakanKorlapTextView)
    TextView uangMakanKorlapTextView;

    @BindView(R.id.gajiAdminTextView)
    TextView gajiAdminTextView;

    @BindView(R.id.uangMakanAdminTextView)
    TextView uangMakanAdminTextView;

    @BindView(R.id.gajiStafTextView)
    TextView gajiStafTextView;

    @BindView(R.id.uangMakanStafTextView)
    TextView uangMakanStafTextView;

    @BindView(R.id.uangLemburDriverTextView)
    TextView uangLemburDriverTextView;

    @BindView(R.id.uangLemburHelperTextView)
    TextView uangLemburHelperTextView;

    @BindView(R.id.uangLemburKorlapTextView)
    TextView uangLemburKorlapTextView;

    @BindView(R.id.lemburanDriverTextView)
    TextView lemburanDriverTextView;
    @BindView(R.id.lemburanHelperTextView)
    TextView lemburanHelperTextView;
    @BindView(R.id.lemburanKorlapTextView)
    TextView lemburanKorlapTextView;
    @BindView(R.id.uangInapTextView)
    TextView uangInapTextView;
    @BindView(R.id.uangSKRTextView)
    TextView uangSKRTextView;
    @BindView(R.id.uangRitaseTextView)
    TextView uangRitaseTextView;
    @BindView(R.id.uangUpahTextView)
    TextView uangUpahTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proyek_detail);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Proyek Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (getIntent().hasExtra("proyekKode")) {
//            /*Proyek proyek = getIntent().getParcelableExtra("proyek");
            proyekDetail(getIntent().getStringExtra("proyekKode"));
        }
    }

    private void proyekDetail(String kode) {
        showProgressDialog();
        Call<ProyekDetailResponse> service = api.proyekDetail("proyek", kode);

        service.enqueue(new Callback<ProyekDetailResponse>() {
            @Override
            public void onResponse(Call<ProyekDetailResponse> call, Response<ProyekDetailResponse> response) {
                dismissProgressDialog();
                ProyekDetailResponse proyekDetailResponse = response.body();

                if (proyekDetailResponse != null) {
                    ProyekDetailResponse.ProyekDetail proyekDetail = proyekDetailResponse.getProyekDetail();
                    proyekTextView.setText(proyekDetail.getProyek());
                    lokasiTextView.setText(proyekDetail.getLokasiText());
                    jenisTextView.setText(proyekDetail.getTipeText());
                    kendaraanTextView.setText(proyekDetail.getKendaraanText());
                    alamatTextView.setText(proyekDetail.getAlamat());

                    muatInapTextView.setText(proyekDetail.getInvoiceInap() + "%");
                    skrTextView.setText(proyekDetail.getInvoiceSkr() + "%");
                    bpjsTextView.setText(proyekDetail.getInvoiceBpjs() + "%");
                    ritaseTextView.setText(proyekDetail.getInvoiceRitase() + "%");
                    pph23TextView.setText(proyekDetail.getInvoicePph() + "%");
                    ppnTextView.setText(proyekDetail.getInvoicePpn() + "%");
                    insentifPengirimanTextView.setText(proyekDetail.getInvoicePengiriman() + "%");

                    supervisiTextView.setText(proyekDetail.getInvoiceSupervisi());
                    umkTextView.setText(proyekDetail.getInvoiceUmk());
                    seragamTextView.setText(proyekDetail.getInvoiceSeragam());
                    thrTextView.setText(proyekDetail.getInvoiceThr());
                    feeKorlapTextView.setText(proyekDetail.getInvoiceFeeKorlap() + "%");
                    feeDriverTextView.setText(proyekDetail.getInvoiceFeeDriver() + "%");
                    feeHelperTextView.setText(proyekDetail.getInvoiceFeeHelper() + "%");
                    gajiDriverTextView.setText(proyekDetail.getGajiDriver());
                    uangMakanDriverTextView.setText(proyekDetail.getMakanDriver());
                    gajiHelperTextView.setText(proyekDetail.getGajiHelper());
                    uangMakanHelperTextView.setText(proyekDetail.getMakanHelper());
                    gajiKorlapTextView.setText(proyekDetail.getGajiKorlap());
                    uangMakanKorlapTextView.setText(proyekDetail.getMakanKorlap());
                    gajiAdminTextView.setText(proyekDetail.getGajiAdmin());
                    uangMakanAdminTextView.setText(proyekDetail.getMakanAdmin());
                    gajiStafTextView.setText(proyekDetail.getGajiStaf());
                    uangMakanStafTextView.setText(proyekDetail.getMakanStaf());
                    uangLemburDriverTextView.setText(proyekDetail.getLemburDriver());
                    uangLemburHelperTextView.setText(proyekDetail.getLemburHelper());
                    uangLemburKorlapTextView.setText(proyekDetail.getLemburKorlap());
                    lemburanDriverTextView.setText(proyekDetail.getLemburanDriver());
                    lemburanHelperTextView.setText(proyekDetail.getLemburanHelper());
                    lemburanKorlapTextView.setText(proyekDetail.getLemburanKorlap());
                    uangInapTextView.setText(proyekDetail.getUangInap());
                    uangSKRTextView.setText(proyekDetail.getUangSKR());
                    uangRitaseTextView.setText(proyekDetail.getUangRitase());
                    uangUpahTextView.setText(proyekDetail.getUangUpah());
                } else {
                    toast(Constants.SERVER_RESPONSE_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ProyekDetailResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
