package com.indonesia.gtu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.Utility;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BarcodeScannerActivity extends BaseActivity {

    @BindView(R.id.barcodeView)
    DecoratedBarcodeView barcodeView;

    BeepManager beepManager;
    String lastText;

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if (result.getText() == null || result.getText().equals(lastText)) {
                // Prevent duplicate scans
                // System.out.println("result = " + result);
                return;
            }

            lastText = result.getText();
            barcodeView.setStatusText(result.getText());
            beepManager.playBeepSoundAndVibrate();

            Intent intent = new Intent();
            intent.putExtra("id", lastText);
            setResult(RESULT_OK, intent);
            finish();
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barode_scanner);
        ButterKnife.bind(this);

        barcodeView.decodeContinuous(callback);
        beepManager = new BeepManager(this);
        Utility.checkPermissions(BarcodeScannerActivity.this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        barcodeView.resume();
        barcodeView.setStatusText("");
        lastText = "";
    }

    @Override
    protected void onPause() {
        super.onPause();
        barcodeView.pause();
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }
}
