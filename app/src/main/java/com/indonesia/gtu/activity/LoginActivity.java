package com.indonesia.gtu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.response.LoginResponse;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.google.firebase.iid.FirebaseInstanceId;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.usernameEditText)
    EditText usernameEditText;

    @BindView(R.id.passwordEditText)
    EditText passwordEditText;

    @OnClick(R.id.loginButton)
    public void loginButton(Button button) {
        if (Utility.isEmpty(usernameEditText)) {
            toast("Username tidak boleh kosong");
        } else if (Utility.isEmpty(passwordEditText)) {
            toast("Password tidak boleh kosong");
        } else {
            loginProcess();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (sessionManager.isAuthorized()) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        Utility.checkPermissions(LoginActivity.this);

        /*usernameEditText.setText("sysdev");
        passwordEditText.setText("sysdev");*/

        /*usernameEditText.setText("klien");
        passwordEditText.setText("klien");*/

        /*usernameEditText.setText("bunu");
        passwordEditText.setText("bunu");*/

        /*usernameEditText.setText("supirman");
        passwordEditText.setText("supirman");*/

    }

    private void loginProcess() {
        showProgressDialog();

        Call<LoginResponse> service = api.login(Utility.getText(usernameEditText), Utility.getText(passwordEditText));

        service.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                dismissProgressDialog();
                LoginResponse loginResponse = response.body();

                if (loginResponse != null) {
                    if (loginResponse.getStatus().equalsIgnoreCase("1")) {
                        initializeSession(loginResponse);
                    } else {
                        toast("Login Gagal");
                    }
                } else {
                    toast("Login Gagal");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Login Gagal");
            }
        });
    }

    void initializeSession(LoginResponse loginResponse) {
        sessionManager.authorize();
        HashMap<String, String> values = new HashMap<String, String>();
        if (loginResponse.getUser().getAkun() != null) {
            values.put(SessionManager.AKUN, loginResponse.getUser().getAkun());
        }
        if (loginResponse.getUser().getBio() != null) {
            values.put(SessionManager.BIO, loginResponse.getUser().getBio());
        }
        if (loginResponse.getUser().getId() != null) {
            values.put(SessionManager.ID, loginResponse.getUser().getId());
        }
        if (loginResponse.getUser().getOtoritas() != null) {
            values.put(SessionManager.OTORITAS, loginResponse.getUser().getOtoritas());
        }

        if (loginResponse.getUser().getProyek() != null) {
            values.put(SessionManager.DEFAULT_PROYEK_ID, loginResponse.getUser().getProyek());
        }

        sessionManager.putString(values);

//        updateToken(FirebaseInstanceId.getInstance().getToken());
        startActivity(new Intent(LoginActivity.this, MainActivity.class));

        finish();
    }

    @Override
    public void onBackPressed() {
        this.moveTaskToBack(true);
    }
}
