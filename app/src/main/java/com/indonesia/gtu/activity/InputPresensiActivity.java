package com.indonesia.gtu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.fragment.ChooseFleFragment;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.response.BiodataResponse;
import com.indonesia.gtu.response.SimpanResponse;

import java.io.File;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputPresensiActivity extends BaseActivity implements ChooseFleFragment.BottomSheetFileChooserListener {

    String kodeProyek, kodeKaryawan;

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.labelLampiranTextView)
    TextView labelLampiranTextView;

    @BindView(R.id.lampiranTextView)
    TextView lampiranTextView;

    @BindView(R.id.karyawanTextView)
    TextView karyawanTextView;

    @BindView(R.id.keteranganEditText)
    EditText keteranganEditText;

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        if (!sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            chooseProyek();
        }
    }

    void chooseProyek() {
        Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
    }

    @OnClick(R.id.lampiranTextView)
    public void lampiranTextViewClick(TextView textView) {
        ChooseFleFragment chooseFleFragment = new ChooseFleFragment();
        chooseFleFragment.show(getSupportFragmentManager(), "chooseFleFragment");
        chooseFleFragment.setListener(this);
    }

    @OnClick(R.id.karyawanTextView)
    public void karyawanTextViewClick(TextView textView) {
        if (Utility.isEmpty(proyekTextView)) {
            chooseProyek();
        } else {
            if (hadirRadioButton.isChecked()) {
                Intent intent = new Intent(getApplicationContext(), BarcodeScannerActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(intent, Constants.REQUEST_ID_QR_CODE);
            } else {
                Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                        .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT)
                        .putExtra("proyekId", kodeProyek)
                        .putExtra("proyekText", Utility.getText(proyekTextView));
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(intent, Constants.REQUEST_ID_KARYAWAN);
            }
        }
    }

    @OnClick(R.id.simpanButton)
    public void simpanButton(Button button) {
        if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
        } else if (Utility.isEmpty(karyawanTextView)) {
            toast("Karyawan tidak boleh kosong");
        } /*else if ((izinRadioButton.isChecked() || sakitRadioButton.isChecked()) && Utility.isEmpty(lampiranTextView)) {
            toast("Lmmpiran boleh kosong");
        } */ else {
            presensiProcess();
        }
    }

    @BindView(R.id.statusRadioGroup)
    RadioGroup statusRadioGroup;

    @BindView(R.id.hadirRadioButton)
    RadioButton hadirRadioButton;

    @BindView(R.id.izinRadioButton)
    RadioButton izinRadioButton;

    @BindView(R.id.sakitRadioButton)
    RadioButton sakitRadioButton;

    @OnClick({R.id.hadirRadioButton, R.id.izinRadioButton, R.id.sakitRadioButton})
    public void onRadioButtonClicked(RadioButton radioButton) {
        // Is the button now checked?
        boolean checked = radioButton.isChecked();

        kodeKaryawan = "";
        karyawanTextView.setText("");

        // Check which radio button was clicked
        switch (radioButton.getId()) {
            case R.id.hadirRadioButton:
                if (checked) {
                    karyawanTextView.setHint("Scan Karyawan");
                    labelLampiranTextView.setVisibility(View.GONE);
                    lampiranTextView.setVisibility(View.GONE);
                }
                break;
            case R.id.izinRadioButton:
                if (checked) {
                    karyawanTextView.setHint("Pilih Karyawan");
                    labelLampiranTextView.setVisibility(View.VISIBLE);
                    lampiranTextView.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.sakitRadioButton:
                if (checked) {
                    karyawanTextView.setHint("Pilih Karyawan");
                    labelLampiranTextView.setVisibility(View.VISIBLE);
                    lampiranTextView.setVisibility(View.VISIBLE);
                }
                break;
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_presensi);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Input Absensi");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            kodeProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
            proyekTextView.setText(sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_NAME));
        }
    }

    private void presensiProcess() {
        showProgressDialog();

        /*model-input↵:presensi
        action-input:1
        key-input:0
        kode-input:3c1f3699c741bf5cea7cc9f373jms215
        terpakai-input:1
        status-input:hadir
        proyek-input:0d4dd841d8a81110adb3d05dbd334f00
        biodata-input:60fcafc488fa932750e8f08fba9fa87d
        waktu-input:18/06/2019 16:30
        keterangan-input:*/

        int selectedId = statusRadioGroup.getCheckedRadioButtonId();
        RadioButton radioGenderButton = findViewById(selectedId);

        final String UUID = Utility.generateUUID();
        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "presensi");
        map.put("action-input", "1");
        map.put("key-input", "0");
        map.put("kode-input", UUID);
        map.put("terpakai-input", "1");
        map.put("status-input", radioGenderButton.getText().toString().toLowerCase());
        map.put("proyek-input", kodeProyek);
        map.put("biodata-input", kodeKaryawan);
        map.put("waktu-input", Utility.now("dd/MM/yyyy HH:mm"));
        map.put("keterangan-input", Utility.getText(keteranganEditText));

        Call<SimpanResponse> service = api.simpan(map);

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast("Absensi Berhasil");
                        if (Utility.isEmpty(lampiranTextView)) {
                            finish();
                        } else {
                            uploadLampiran(UUID);
                        }
                    } else {
                        toast("Absensi Gagal");
                    }
                } else {
                    toast("Absensi Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Absensi Gagal");
            }
        });
    }

    private void uploadLampiran(String UUID) {
        showProgressDialog("Upload Lampiran ...");

        File compressFile = Utility.compressFile(getApplicationContext(), lampiranFile);
        String gambar = Utility.imageToBase64(compressFile);
        Call<SimpanResponse> service = api.uploadGambar(UUID, gambar, "presensi");

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast(simpanResponse.getPesan());
                        finish();
                    } else {
                        toast("Upload Gagal");
                    }
                } else {
                    toast("Upload Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Upload Gagal");
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));

                kodeKaryawan = "";
                karyawanTextView.setText("");
            } else if (requestCode == Constants.REQUEST_ID_QR_CODE) {
                kodeKaryawan = data.getStringExtra("id");
                biodata(kodeKaryawan);
            } else if (requestCode == Constants.REQUEST_ID_KARYAWAN) {
                kodeKaryawan = data.getStringExtra("id");
                karyawanTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    private void biodata(String bio) {
        showProgressDialog();
        Call<BiodataResponse> service = api.biodata("bio", bio);

        service.enqueue(new Callback<BiodataResponse>() {
            @Override
            public void onResponse(Call<BiodataResponse> call, Response<BiodataResponse> response) {
                dismissProgressDialog();
                BiodataResponse biodataResponse = response.body();

                if (biodataResponse != null) {
                    karyawanTextView.setText(biodataResponse.getBiodata().getNama());
                } else {
                    toast(Constants.SERVER_RESPONSE_ERROR);
                }
            }

            @Override
            public void onFailure(Call<BiodataResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
            }
        });
    }

    File lampiranFile;

    @Override
    public void onFileSelected(File imageFile) {
        lampiranTextView.setText(imageFile.getName());
        lampiranFile = imageFile;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
