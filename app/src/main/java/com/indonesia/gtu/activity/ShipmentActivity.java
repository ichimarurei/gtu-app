package com.indonesia.gtu.activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.indonesia.gtu.R;
import com.indonesia.gtu.adapter.ShipmentAdapter;
import com.indonesia.gtu.fragment.ProyekFilterBottomSheetDialog;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.model.Shipment;
import com.indonesia.gtu.response.ShipmentResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShipmentActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener
        , ProyekFilterBottomSheetDialog.BottomSheetFilterListener {

    String proyekId = "", proyekText = "";

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.filterConstraintLayout)
    public void filterConstraintLayoutCLick(ConstraintLayout constraintLayout) {
        openFilterBottomSheet();
    }

    @BindView(R.id.filterConstraintLayout)
    ConstraintLayout filterConstraintLayout;

    SearchView searchView;
    List<Shipment> shipmentList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipment);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Data Shipment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            filterConstraintLayout.setVisibility(View.GONE);
        } else {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0 && filterConstraintLayout.getVisibility() == View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animHide);
                    } else if (dy < 0 && filterConstraintLayout.getVisibility() != View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animShow);
                    }
                }
            });
        }

        getShipmentData();
    }

    @Override
    public void onRefresh() {
        searchView.setQuery("", false);
        searchView.requestFocus();
        getShipmentData();
    }

    void getShipmentData() {
        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            proyekId = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
        }

        if (proyekId.length() > 0) {
            shipmentList = new ArrayList<>();
            swipeRefreshLayout.setRefreshing(true);
            recyclerView.setAdapter(null);

            Call<ShipmentResponse> service = api.getShipmentList(proyekId);

            service.enqueue(new Callback<ShipmentResponse>() {
                @Override
                public void onResponse(Call<ShipmentResponse> call, Response<ShipmentResponse> response) {
                    swipeRefreshLayout.setRefreshing(false);
                    ShipmentResponse shipmentResponse = response.body();
                    shipmentList = shipmentResponse.getShipmentList();
                    setAdapterDataRecycleView("");
                }

                @Override
                public void onFailure(Call<ShipmentResponse> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    call.cancel();
                    toast(Constants.SERVER_RESPONSE_ERROR);
                }
            });
        }
    }

    private void setAdapterDataRecycleView(String keyword) {
        final List<Shipment> filterList = new ArrayList<>();

        if (keyword.length() > 0) {
            for (Shipment shipment : shipmentList) {
                if (shipment.getBiodata().toLowerCase().contains(keyword)) {
                    filterList.add(shipment);
                }
            }
        } else {
            filterList.addAll(shipmentList);
        }

        recyclerView.setAdapter(new ShipmentAdapter(filterList, getApplicationContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                        /*startActivity(new Intent(getApplicationContext(), ShipmentDetailActivity.class)
                                .putExtra("newsId", newsList.get(position).getShipmentId()));*/
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    void openFilterBottomSheet() {
        Bundle bundle = new Bundle();
        bundle.putString("proyekId", proyekId);
        bundle.putString("proyekText", proyekText);

        ProyekFilterBottomSheetDialog bottomSheet = new ProyekFilterBottomSheetDialog();
        bottomSheet.setArguments(bundle);
        bottomSheet.setListener(this);
        bottomSheet.show(getSupportFragmentManager(), "proyekFilterBottomSheet");
    }

    @Override
    public void onButtonFilterClicked(String proyekId, String proyekText) {
        this.proyekId = proyekId;
        this.proyekText = proyekText;
        getShipmentData();
    }

    @Override
    public void onButtonrResetClicked() {
        this.proyekId = "";
        this.proyekText = "";
        getShipmentData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setAdapterDataRecycleView(newText);
                return true;
            }
        });

        return true;
    }
}
