package com.indonesia.gtu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.response.SimpanResponse;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputShipmentActivity extends BaseActivity {

    String kodeProyek, kodeKaryawan;

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.karyawanTextView)
    TextView karyawanTextView;

    @BindView(R.id.insentifPengirimanEditText)
    EditText insentifPengirimanEditText;

    @BindView(R.id.muatInapEditText)
    EditText muatInapEditText;

    @BindView(R.id.skrEditText)
    EditText skrEditText;

    @BindView(R.id.ritase2EditText)
    EditText ritase2EditText;

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        if (!sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                    .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
        }
    }

    @OnClick(R.id.karyawanTextView)
    public void karyawanTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_KARYAWAN);
    }

    @OnClick(R.id.simpanButton)
    public void simpanButton(Button button) {
        if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
        } else if (Utility.isEmpty(karyawanTextView)) {
            toast("Karyawan tidak boleh kosong");
        } else if (Utility.isEmpty(insentifPengirimanEditText)) {
            toast("Insentif Pengiriman tidak boleh kosong");
        } else if (Utility.isEmpty(muatInapEditText)) {
            toast("Muat Inap tidak boleh kosong");
        } else if (Utility.isEmpty(skrEditText)) {
            toast("SKR tidak boleh kosong");
        } else if (Utility.isEmpty(ritase2EditText)) {
            toast("Ritase 2 tidak boleh kosong");
        } else {
            shipmentProcess();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_shipment);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Input Shipment");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            kodeProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
            proyekTextView.setText(sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_NAME));
        }
    }

    private void shipmentProcess() {
        showProgressDialog();

        /*model-input:qty
        action-input:1
        key-input:0
        kode-input:3c1f3699c741bf5cea7cc9f373jms215
        proyek-input:3c1f3699c741bf5cea7cc9f3736mi215
        biodata-input:3c1f3699c741bf5cea7cc9f3736mi215
        insentif-input:0
        inap-input:1
        skr-input:2
        ritase-input:0
        waktu-input:2019-06-18 17:42:08
        terpakai-input:1*/

        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "qty");
        map.put("action-input", "1");
        map.put("key-input", "0");
        map.put("kode-input", Utility.generateUUID());
        map.put("proyek-input", kodeProyek);
        map.put("biodata-input", kodeKaryawan);
        map.put("insentif-input", Utility.getText(insentifPengirimanEditText));
        map.put("inap-input", Utility.getText(muatInapEditText));
        map.put("skr-input", Utility.getText(skrEditText));
        map.put("ritase-input", Utility.getText(ritase2EditText));
        map.put("waktu-input", Utility.now());
        map.put("terpakai-input", "1");

        Call<SimpanResponse> service = api.simpan(map);

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast("Kasbon Berhasil");
                        finish();
                    } else {
                        toast("Kasbon Gagal");
                    }
                } else {
                    toast("Kasbon Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Kasbon Gagal");
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));
            } else if (requestCode == Constants.REQUEST_ID_KARYAWAN) {
                kodeKaryawan = data.getStringExtra("id");
                karyawanTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
