package com.indonesia.gtu.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.response.BiodataResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KaryawanDetailActivity extends BaseActivity {
    @BindView(R.id.IDPegawaiTextView)
    TextView IDPegawaiTextView;

    @BindView(R.id.ktpTextView)
    TextView ktpTextView;

    @BindView(R.id.npwpTextView)
    TextView npwpTextView;

    @BindView(R.id.bpjsTextView)
    TextView bpjsTextView;

    @BindView(R.id.namaTextView)
    TextView namaTextView;

    @BindView(R.id.kelaminTextView)
    TextView kelaminTextView;

    @BindView(R.id.agamaTextView)
    TextView agamaTextView;

    @BindView(R.id.tempatLahirTextView)
    TextView tempatLahirTextView;

    @BindView(R.id.tanggalLahirTextView)
    TextView tanggalLahirTextView;

    @BindView(R.id.teleponTextView)
    TextView teleponTextView;

    @BindView(R.id.emailTextView)
    TextView emailTextView;

    @BindView(R.id.statusPernikahanTextView)
    TextView statusPernikahanTextView;

    @BindView(R.id.jumlahAnakTextView)
    TextView jumlahAnakTextView;

    @BindView(R.id.pendidikanTextView)
    TextView pendidikanTextView;

    @BindView(R.id.alamatTextView)
    TextView alamatTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karyawan_detail);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Karyawan Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent().hasExtra("karyawanKode")) {
            karyawanDetail(getIntent().getStringExtra("karyawanKode"));
        }
    }

    private void karyawanDetail(String kode) {
        showProgressDialog();
        Call<BiodataResponse> service = api.biodata("bio", kode);

        service.enqueue(new Callback<BiodataResponse>() {
            @Override
            public void onResponse(Call<BiodataResponse> call, Response<BiodataResponse> response) {
                dismissProgressDialog();
                BiodataResponse biodataResponse = response.body();

                if (biodataResponse != null) {
                    BiodataResponse.Biodata biodata = biodataResponse.getBiodata();

                    IDPegawaiTextView.setText(biodata.getId());
                    ktpTextView.setText(biodata.getKtp());
                    npwpTextView.setText(biodata.getNpwp());
                    bpjsTextView.setText(biodata.getBpjs());
                    namaTextView.setText(biodata.getNama());
                    kelaminTextView.setText(biodata.getKelamin());
                    agamaTextView.setText(biodata.getAgama());
                    tempatLahirTextView.setText(biodata.getTempatLahir());
                    tanggalLahirTextView.setText(biodata.getTanggalLahir());
                    teleponTextView.setText(biodata.getTelepon());
                    emailTextView.setText(biodata.getEmail());
                    statusPernikahanTextView.setText(biodata.getNikah());
                    jumlahAnakTextView.setText(biodata.getAnak());
                    pendidikanTextView.setText(biodata.getPendidikan());
                    alamatTextView.setText(biodata.getAlamat());
                } else {
                    toast(Constants.SERVER_RESPONSE_ERROR);
                }
            }

            @Override
            public void onFailure(Call<BiodataResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
