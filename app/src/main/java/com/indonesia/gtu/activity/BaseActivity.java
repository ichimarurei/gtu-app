package com.indonesia.gtu.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.google.gson.Gson;
import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.ApiInterface;
import com.indonesia.gtu.helper.ConnectionDetector;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;

public class BaseActivity extends AppCompatActivity {
    public ApiInterface api;
    public ConnectionDetector cd;
    public ProgressDialog pDialog;
    public SessionManager sessionManager;
    public Gson gson;
    public AlphaAnimation bottomMenuAlphaAnimation = new AlphaAnimation(1F, 0.8F);
    public Animation animShow;
    public Animation animHide;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        sessionManager = new SessionManager(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        gson = new Gson();
        animShow = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.view_show);
        animHide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.view_hide);
        api = Utility.getInterfaceService();
        Utility.updateGPS(sessionManager, api, this);
    }

    public void toast(String message) {
        Utility.makeToast(getApplicationContext(), message);
    }

    public void showProgressDialog(String message) {
        pDialog = new ProgressDialog(BaseActivity.this);
        pDialog.setMessage(message);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void showProgressDialog() {
        pDialog = new ProgressDialog(BaseActivity.this);
        pDialog.setMessage(Constants.DEFAULT_LOADING_MESSAGE);
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void dismissProgressDialog() {
        if (pDialog != null) {
            if (pDialog.isShowing()) {
                pDialog.dismiss();
            }
        }
    }

    public boolean requestPermission(String permission) {
        boolean isGranted = ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
        if (!isGranted) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{permission},
                    Constants.REQUEST_ID_READ_WRITE_STORAGE);
        }
        return isGranted;
    }

    public void isPermissionGranted(boolean isGranted, String permission) {

    }

    public void makeFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_ID_READ_WRITE_STORAGE:
                isPermissionGranted(grantResults[0] == PackageManager.PERMISSION_GRANTED, permissions[0]);
                break;
        }
    }


    /*void validateSession() {
        if (!sessionManager.isAuthorized()) {
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(i);
            finish();
        }
    }*/

    void startActivityIntent(Class c) {
        startActivity(new Intent(getBaseContext(), c));
    }
}
