package com.indonesia.gtu.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.indonesia.gtu.R;
import com.indonesia.gtu.adapter.ArsipAdapter;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.model.Arsip;
import com.indonesia.gtu.response.ArsipResponse;
import com.indonesia.gtu.response.RekapDetailResponse;
import com.indonesia.gtu.response.SimpanResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArsipActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    String rekapKode = "", tahap = "", rekapType = "";

    RekapDetailResponse rekapDetailResponse;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.actionCardView)
    CardView actionCardView;

    SearchView searchView;
    List<Arsip> arsipList = new ArrayList<>();

    @OnClick(R.id.tolakButton)
    public void tolakButton(Button button) {
        approvalProcess("tolak");
    }

    @OnClick(R.id.terimaButton)
    public void terimaButton(Button button) {
        approvalProcess("setuju");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arsip);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Data Rekap Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rekapKode = getIntent().getStringExtra("rekapKode");
        tahap = getIntent().getStringExtra("tahap");
        rekapType = getIntent().getStringExtra("rekapType");

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getArsip();
        getRekapDetail();
    }

    @Override
    public void onRefresh() {
        searchView.setQuery("", false);
        searchView.requestFocus();
        getArsip();
    }

    void getArsip() {
        arsipList = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        recyclerView.setAdapter(null);

        Call<ArsipResponse> service = api.getArsip(rekapKode, tahap);

        service.enqueue(new Callback<ArsipResponse>() {
            @Override
            public void onResponse(Call<ArsipResponse> call, Response<ArsipResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                ArsipResponse arsipResponse = response.body();
                arsipList = arsipResponse.getRekapDetailList();
                setAdapterDataRecycleView("");
            }

            @Override
            public void onFailure(Call<ArsipResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                call.cancel();
                toast(Constants.SERVER_RESPONSE_ERROR);
            }
        });
    }

    private void setAdapterDataRecycleView(String keyword) {
        final List<Arsip> filterList = new ArrayList<>();

        if (keyword.length() > 0) {
            for (Arsip rekapDetail : arsipList) {
                if (rekapDetail.getBiodata().toLowerCase().contains(keyword)) {
                    filterList.add(rekapDetail);
                }
            }
        } else {
            filterList.addAll(arsipList);
        }

        recyclerView.setAdapter(new ArsipAdapter(filterList, getApplicationContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                        /*startActivity(new Intent(getApplicationContext(), RekapDetailDetailActivity.class)
                                .putExtra("newsId", newsList.get(position).getRekapDetailId()));*/
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setAdapterDataRecycleView(newText);
                return true;
            }
        });

        return true;
    }

    void getRekapDetail() {
        Call<RekapDetailResponse> service = api.rekapDetail("rekap", rekapKode);

        service.enqueue(new Callback<RekapDetailResponse>() {
            @Override
            public void onResponse(Call<RekapDetailResponse> call, Response<RekapDetailResponse> response) {
                rekapDetailResponse = response.body();

                if (rekapDetailResponse.getRekapDetail() != null) {
                    if (rekapDetailResponse.getRekapDetail().getStatus().equalsIgnoreCase("ajuan")) {
                        actionCardView.setVisibility(View.VISIBLE);
                    } else {
                        actionCardView.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<RekapDetailResponse> call, Throwable t) {
                call.cancel();
                toast(Constants.SERVER_RESPONSE_ERROR);
            }
        });
    }

    private void approvalProcess(String status) {
        showProgressDialog();

       /* - model-input = "rekap" (fixed)
                - action-input = 2 (fixed)
                - terpakai-input = 1 (fixed)
                - jenis-input = opsi antara "presensi" ma "lembur"
                - key-input = key dari response detail
                - kode-input = kode dari response detail
                - proyek-input = proyek dari response detail
                - dari-input = dari dari response detail
                - hingga-input = hingga dari response detail
                - waktu-input = waktu dari response detail
                - status-input = opsi antara "setuju" ma "tolak" >>> berdasarkan aksi tombolnya*/

        RekapDetailResponse.RekapDetail rekapDetail = rekapDetailResponse.getRekapDetail();

        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "rekap");
        map.put("action-input", "2");
        map.put("terpakai-input", "1");
        map.put("jenis-input", rekapType);
        map.put("key-input", rekapDetail.getKey());
        map.put("kode-input", rekapDetail.getKode());
        map.put("proyek-input", rekapDetail.getProyek());
        map.put("dari-input", rekapDetail.getDari());
        map.put("hingga-input", rekapDetail.getHingga());
        map.put("waktu-input", rekapDetail.getWaktu());
        map.put("status-input", status);

        Call<SimpanResponse> service = api.simpan(map);

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast(simpanResponse.getPesan());
                        finish();
                    } else {
                        toast(simpanResponse.getPesan());
                    }
                } else {
                    toast("Approval Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Approval Gagal");
            }
        });
    }
}
