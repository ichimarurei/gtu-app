package com.indonesia.gtu.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.model.Termin;
import com.indonesia.gtu.response.SimpanResponse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputKasbonActivity extends BaseActivity {

    String kodeProyek, kodeKaryawan;

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.karyawanTextView)
    TextView karyawanTextView;

    @BindView(R.id.tanggalKasbonTextView)
    TextView tanggalKasbonTextView;

    @BindView(R.id.nominalEditText)
    EditText nominalEditText;

    @BindView(R.id.terminSpinner)
    Spinner terminSpinner;

    @BindView(R.id.perihalEditText)
    EditText perihalEditText;

    @OnClick(R.id.tanggalKasbonTextView)
    public void tanggalKasbonTextViewClick(TextView textView) {
        openCalendar();
    }

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        if (!sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                    .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
        }
    }

    @OnClick(R.id.karyawanTextView)
    public void karyawanTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_KARYAWAN);
    }

    @OnClick(R.id.simpanButton)
    public void simpanButton(Button button) {
        if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
        } else if (Utility.isEmpty(karyawanTextView)) {
            toast("Karyawan tidak boleh kosong");
        } else if (Utility.isEmpty(tanggalKasbonTextView)) {
            toast("Tanggal Kasbon tidak boleh kosong");
        } else if (Utility.isEmpty(nominalEditText)) {
            toast("Nominal tidak boleh kosong");
        } else if (terminList.get((int) terminSpinner.getSelectedItemId()).getId().equalsIgnoreCase("")) {
            toast("Termin tidak boleh kosong");
        } else {
            kasbonProcess();
        }
    }

    ArrayList<Termin> terminList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_kasbon);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Input Kasbon");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            kodeProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
            proyekTextView.setText(sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_NAME));
        }

        setDataTermin();
    }

    private void setDataTermin() {
        terminList.add(new Termin("", "-- Pilih Termin --"));
        terminList.add(new Termin("1", "1x Termin Pemotongan"));
        terminList.add(new Termin("2", "2x Termin Pemotongan"));
        terminList.add(new Termin("3", "3x Termin Pemotongan"));
        terminList.add(new Termin("4", "4x Termin Pemotongan"));
        terminList.add(new Termin("5", "5x Termin Pemotongan"));
        terminList.add(new Termin("6", "6x Termin Pemotongan"));

        ArrayAdapter<Termin> adapter = new ArrayAdapter<Termin>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, terminList);
        terminSpinner.setAdapter(adapter);

        terminSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void kasbonProcess() {
        showProgressDialog();

        /*model-input:kasbon
            action-input:1
            key-input:0
            kode-input:3c1f3699c741bf5cea7cc9f373jms215
            proyek-input:3c1f3699c741bf5cea7cc9f3736mi215
            pegawai-input:3c1f3699c741bf5cea7cc9f3736mi215
            pemohon-input:3c1f3699c741bf5cea7cc9f3736mi215
            status-input:ajuan
            tanggal-input:20/07/2019
            perihal-input:Test
            terpakai-input:1*/

        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "kasbon");
        map.put("action-input", "1");
        map.put("key-input", "0");
        map.put("kode-input", Utility.generateUUID());
        map.put("proyek-input", kodeProyek);
        map.put("pegawai-input", kodeKaryawan);
        map.put("pemohon-input", sessionManager.retrieve(SessionManager.BIO));
        map.put("status-input", "ajuan");
        map.put("tanggal-input", Utility.getText(tanggalKasbonTextView));
        map.put("nominal-input", Utility.getText(nominalEditText));
        map.put("perihal-input", Utility.getText(perihalEditText));
        map.put("terpakai-input", "1");
        map.put("cicil-input", terminList.get((int) terminSpinner.getSelectedItemId()).getId());

        Call<SimpanResponse> service = api.simpan(map);

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast("Kasbon Berhasil");
                        finish();
                    } else {
                        toast("Kasbon Gagal");
                    }
                } else {
                    toast("Kasbon Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Kasbon Gagal");
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));
            } else if (requestCode == Constants.REQUEST_ID_KARYAWAN) {
                kodeKaryawan = data.getStringExtra("id");
                karyawanTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    void openCalendar() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(InputKasbonActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String sMonth = String.valueOf(monthOfYear);
                        if (sMonth.length() == 1) {
                            sMonth = "0" + (Integer.parseInt(sMonth) + 1);
                            sMonth = sMonth.substring(sMonth.length() - 2);
                        }

                        String sDay = String.valueOf(dayOfMonth);
                        if (sDay.length() == 1) {
                            sDay = "0" + sDay;
                        }

                        tanggalKasbonTextView.setText(sDay + "/" + sMonth + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
