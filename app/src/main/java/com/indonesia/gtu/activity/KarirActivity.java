package com.indonesia.gtu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.adapter.KarirSpAdapter;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.model.Termin;
import com.indonesia.gtu.response.KarirResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KarirActivity extends BaseActivity {

    String kodeProyek, kodeKaryawan;
    ArrayList<Termin> bulanList = new ArrayList<>();


    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.karyawanTextView)
    TextView karyawanTextView;

    @BindView(R.id.bulanSpinner)
    Spinner bulanSpinner;

    @BindView(R.id.nikTextView)
    TextView nikTextView;

    @BindView(R.id.namaTextView)
    TextView namaTextView;

    @BindView(R.id.jabatanTextView)
    TextView jabatanTextView;

    @BindView(R.id.hadirTextView)
    TextView hadirTextView;

    @BindView(R.id.izinTextView)
    TextView izinTextView;

    @BindView(R.id.alfaTextView)
    TextView alfaTextView;

    @BindView(R.id.sakitTextView)
    TextView sakitTextView;

    @BindView(R.id.lemburTextView)
    TextView lemburTextView;

    @BindView(R.id.statistikHadirTextView)
    TextView statistikHadirTextView;

    @BindView(R.id.statistikIzinTextView)
    TextView statistikIzinTextView;

    @BindView(R.id.statistikAlfaTextView)
    TextView statistikAlfaTextView;

    @BindView(R.id.statistikSakitTextView)
    TextView statistikSakitTextView;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        if (!sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                    .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
        }
    }

    @OnClick(R.id.karyawanTextView)
    public void karyawanTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_KARYAWAN);
    }

    @OnClick(R.id.lihatButton)
    public void lihatButton(Button button) {
        if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
        } else if (Utility.isEmpty(karyawanTextView)) {
            toast("Karyawan tidak boleh kosong");
        } else {
            getDataKarir();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karir);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Data Karir");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            kodeProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
            proyekTextView.setText(sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_NAME));
        }

        setDataBulan();
        bulanSpinner.setPrompt("-- Pilih Bulan --");

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    private void getDataKarir() {
        showProgressDialog();

        Call<KarirResponse> service = api.karir(kodeProyek, kodeKaryawan,
                bulanList.get((int) bulanSpinner.getSelectedItemId()).getId());
        service.enqueue(new Callback<KarirResponse>() {
            @Override
            public void onResponse(Call<KarirResponse> call, Response<KarirResponse> response) {
                dismissProgressDialog();

                KarirResponse karirResponse = response.body();
                KarirResponse.Karir karir = karirResponse.getKarir();

                if (karir != null) {
                    nikTextView.setText(karir.getNik());
                    namaTextView.setText(karir.getNama());
                    jabatanTextView.setText(karir.getJabatan());

                    hadirTextView.setText(karir.getHadir());
                    izinTextView.setText(karir.getIzin());
                    alfaTextView.setText(karir.getAlfa());
                    sakitTextView.setText(karir.getSakit());
                    lemburTextView.setText(karir.getLembur());

                    statistikHadirTextView.setText(karir.getStatistik().getHadir());
                    statistikIzinTextView.setText(karir.getStatistik().getIzin());
                    statistikAlfaTextView.setText(karir.getStatistik().getAlfa());
                    statistikSakitTextView.setText(karir.getStatistik().getSakit());


                    recyclerView.setAdapter(null);

                    if (karir.getSuratPeringartanList() != null) {
                        if (karir.getSuratPeringartanList().size() > 0) {
                            recyclerView.setAdapter(new KarirSpAdapter(karir.getSuratPeringartanList(), getApplicationContext(), new ClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                }

                                @Override
                                public void onItemLongClick(View view, int position) {

                                }
                            }));
                        }

                    }
                } else {
                    toast(Constants.SERVER_RESPONSE_ERROR);
                }
            }

            @Override
            public void onFailure(Call<KarirResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));
            } else if (requestCode == Constants.REQUEST_ID_KARYAWAN) {
                kodeKaryawan = data.getStringExtra("id");
                karyawanTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    private void setDataBulan() {
        bulanList.add(new Termin("", "-- Pilih Bulan --"));
        bulanList.add(new Termin("01", "Januari"));
        bulanList.add(new Termin("02", "Feburari"));
        bulanList.add(new Termin("03", "Maret"));
        bulanList.add(new Termin("04", "April"));
        bulanList.add(new Termin("05", "Mei"));
        bulanList.add(new Termin("06", "Juni"));
        bulanList.add(new Termin("07", "Juli"));
        bulanList.add(new Termin("08", "Agustus"));
        bulanList.add(new Termin("09", "September"));
        bulanList.add(new Termin("10", "Oktober"));
        bulanList.add(new Termin("12", "November"));
        bulanList.add(new Termin("12", "Desember"));

        ArrayAdapter<Termin> adapter = new ArrayAdapter<Termin>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, bulanList);
        bulanSpinner.setAdapter(adapter);

        bulanSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

}
