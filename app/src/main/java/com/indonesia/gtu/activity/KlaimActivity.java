package com.indonesia.gtu.activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.indonesia.gtu.R;
import com.indonesia.gtu.adapter.KlaimAdapter;
import com.indonesia.gtu.fragment.ProyekFilterBottomSheetDialog;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.model.Klaim;
import com.indonesia.gtu.response.KlaimResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KlaimActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener
        , ProyekFilterBottomSheetDialog.BottomSheetFilterListener {

    String proyekId = "", proyekText = "";

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.filterConstraintLayout)
    public void filterConstraintLayoutCLick(ConstraintLayout constraintLayout) {
        openFilterBottomSheet();
    }

    @BindView(R.id.filterConstraintLayout)
    ConstraintLayout filterConstraintLayout;

    SearchView searchView;
    List<Klaim> klaimList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_klaim);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Data Klaim");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            filterConstraintLayout.setVisibility(View.GONE);
        }

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && filterConstraintLayout.getVisibility() == View.VISIBLE) {
                    filterConstraintLayout.startAnimation(animHide);
                } else if (dy < 0 && filterConstraintLayout.getVisibility() != View.VISIBLE) {
                    filterConstraintLayout.startAnimation(animShow);
                }
            }
        });

        getKlaimData();
    }

    @Override
    public void onRefresh() {
        searchView.setQuery("", false);
        searchView.requestFocus();
        getKlaimData();
    }

    void getKlaimData() {
        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            proyekId = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
        }

        if (proyekId.length() > 0) {
            klaimList = new ArrayList<>();
            swipeRefreshLayout.setRefreshing(true);
            recyclerView.setAdapter(null);

            Call<KlaimResponse> service = api.getKlaimList("klaim", proyekId);

            service.enqueue(new Callback<KlaimResponse>() {
                @Override
                public void onResponse(Call<KlaimResponse> call, Response<KlaimResponse> response) {
                    swipeRefreshLayout.setRefreshing(false);
                    KlaimResponse klaimResponse = response.body();
                    klaimList = klaimResponse.getKlaimList();
                    setAdapterDataRecycleView("");
                }

                @Override
                public void onFailure(Call<KlaimResponse> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    call.cancel();
                    toast(Constants.SERVER_RESPONSE_ERROR);
                }
            });
        }
    }

    private void setAdapterDataRecycleView(String keyword) {
        final List<Klaim> filterList = new ArrayList<>();

        if (keyword.length() > 0) {
            for (Klaim klaim : klaimList) {
                if (klaim.getBiodata().toLowerCase().contains(keyword)) {
                    filterList.add(klaim);
                }
            }
        } else {
            filterList.addAll(klaimList);
        }

        recyclerView.setAdapter(new KlaimAdapter(filterList, getApplicationContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                        /*startActivity(new Intent(getApplicationContext(), KlaimDetailActivity.class)
                                .putExtra("newsId", newsList.get(position).getKlaimId()));*/
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    void openFilterBottomSheet() {
        Bundle bundle = new Bundle();
        bundle.putString("proyekId", proyekId);
        bundle.putString("proyekText", proyekText);

        ProyekFilterBottomSheetDialog bottomSheet = new ProyekFilterBottomSheetDialog();
        bottomSheet.setArguments(bundle);
        bottomSheet.setListener(this);
        bottomSheet.show(getSupportFragmentManager(), "proyekFilterBottomSheet");
    }

    @Override
    public void onButtonFilterClicked(String proyekId, String proyekText) {
        this.proyekId = proyekId;
        this.proyekText = proyekText;
        getKlaimData();
    }

    @Override
    public void onButtonrResetClicked() {
        this.proyekId = "";
        this.proyekText = "";
        getKlaimData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setAdapterDataRecycleView(newText);
                return true;
            }
        });

        return true;
    }
}
