package com.indonesia.gtu.activity;

import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.indonesia.gtu.R;
import com.indonesia.gtu.adapter.KasAdapter;
import com.indonesia.gtu.fragment.ProyekFilterBottomSheetDialog;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.model.KasKecil;
import com.indonesia.gtu.response.KasKecilResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KasKecilActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, ProyekFilterBottomSheetDialog.BottomSheetFilterListener {
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.filterConstraintLayout)
    ConstraintLayout filterConstraintLayout;

    private String proyekId = "", proyekText = "";
    private SearchView searchView;
    private List<KasKecil> kasList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kas_kecil);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Data Kas Kecil");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            filterConstraintLayout.setVisibility(View.GONE);
        }

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && filterConstraintLayout.getVisibility() == View.VISIBLE) {
                    filterConstraintLayout.startAnimation(animHide);
                } else if (dy < 0 && filterConstraintLayout.getVisibility() != View.VISIBLE) {
                    filterConstraintLayout.startAnimation(animShow);
                }
            }
        });

        getData();
    }

    @Override
    public void onRefresh() {
        searchView.setQuery("", false);
        searchView.requestFocus();
        getData();
    }

    @Override
    public void onButtonFilterClicked(String proyekId, String proyekText) {
        this.proyekId = proyekId;
        this.proyekText = proyekText;
        getData();
    }

    @Override
    public void onButtonrResetClicked() {
        this.proyekId = "";
        this.proyekText = "";
        getData();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setAdapterDataRecycleView(newText);
                return true;
            }
        });

        return true;
    }

    @OnClick(R.id.filterConstraintLayout)
    public void filterConstraintLayoutCLick(ConstraintLayout constraintLayout) {
        openFilterBottomSheet();
    }

    private void getData() {
        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            proyekId = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
        }

        if (proyekId.length() > 0) {
            kasList.clear();
            swipeRefreshLayout.setRefreshing(true);
            recyclerView.setAdapter(null);
            Call<KasKecilResponse> service = api.getKasList(proyekId);
            service.enqueue(new Callback<KasKecilResponse>() {
                @Override
                public void onResponse(Call<KasKecilResponse> call, Response<KasKecilResponse> response) {
                    swipeRefreshLayout.setRefreshing(false);
                    KasKecilResponse kasResponse = response.body();
                    kasList = kasResponse.getKasList();
                    setAdapterDataRecycleView("");
                }

                @Override
                public void onFailure(Call<KasKecilResponse> call, Throwable t) {
                    swipeRefreshLayout.setRefreshing(false);
                    toast(Constants.SERVER_RESPONSE_ERROR);
                    call.cancel();
                }
            });
        }
    }

    private void setAdapterDataRecycleView(String keyword) {
        final List<KasKecil> filterList = new ArrayList<>();

        if (keyword.length() > 0) {
            for (KasKecil kasKecil : kasList) {
                if (kasKecil.getPerihal().toLowerCase().contains(keyword.toLowerCase())) {
                    filterList.add(kasKecil);
                }
            }
        } else {
            filterList.addAll(kasList);
        }

        recyclerView.setAdapter(new KasAdapter(filterList, getApplicationContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }

            @Override
            public void onItemLongClick(View view, int position) {
            }
        }));
    }

    private void openFilterBottomSheet() {
        Bundle bundle = new Bundle();
        bundle.putString("proyekId", proyekId);
        bundle.putString("proyekText", proyekText);
        ProyekFilterBottomSheetDialog bottomSheet = new ProyekFilterBottomSheetDialog();
        bottomSheet.setArguments(bundle);
        bottomSheet.setListener(this);
        bottomSheet.show(getSupportFragmentManager(), "proyekFilterBottomSheet");
    }
}
