package com.indonesia.gtu.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.fragment.ChooseFleFragment;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.response.SimpanResponse;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputKlaimActivity extends BaseActivity implements ChooseFleFragment.BottomSheetFileChooserListener {

    String kodeProyek, kodeKaryawan;
    File lampiranFile;

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.karyawanTextView)
    TextView karyawanTextView;

    @BindView(R.id.tanggalKlaimTextView)
    TextView tanggalKlaimTextView;

    @BindView(R.id.lampiranTextView)
    TextView lampiranTextView;

    @BindView(R.id.nominalEditText)
    EditText nominalEditText;

    @BindView(R.id.perihalEditText)
    EditText perihalEditText;

    @OnClick(R.id.lampiranTextView)
    public void lampiranTextViewClick(TextView textView) {
        ChooseFleFragment chooseFleFragment = new ChooseFleFragment();
        chooseFleFragment.show(getSupportFragmentManager(), "chooseFleFragment");
        chooseFleFragment.setListener(this);
    }

    @OnClick(R.id.tanggalKlaimTextView)
    public void tanggalKlaimTextViewClick(TextView textView) {
        openCalendar();
    }

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        if (!sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                    .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
        }

    }

    @OnClick(R.id.karyawanTextView)
    public void karyawanTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_KARYAWAN);
    }

    @OnClick(R.id.simpanButton)
    public void simpanButton(Button button) {
        if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
        } else if (Utility.isEmpty(karyawanTextView)) {
            toast("Karyawan tidak boleh kosong");
        } else if (Utility.isEmpty(tanggalKlaimTextView)) {
            toast("Tanggal Klaim tidak boleh kosong");
        } else if (Utility.isEmpty(nominalEditText)) {
            toast("Nominal tidak boleh kosong");
        } else if (lampiranFile == null) {
            toast("File Lampiran harus dipilih");
        } else {
            klaimProcess();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_klaim);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Input Klaim / Reimburse");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            kodeProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
            proyekTextView.setText(sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_NAME));
        }
    }

    private void klaimProcess() {
        showProgressDialog();

        /*model-input:klaim
            action-input:1
            key-input:0
            kode-input:3c1f3699c741bf5cea7cc9f373jms215
            proyek-input:3c1f3699c741bf5cea7cc9f3736mi215
            pegawai-input:3c1f3699c741bf5cea7cc9f3736mi215
            pemohon-input:3c1f3699c741bf5cea7cc9f3736mi215
            status-input:ajuan
            tanggal-input:20/07/2019
            perihal-input:Test
            terpakai-input:1*/

        final String UUID = Utility.generateUUID();
        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "klaim");
        map.put("action-input", "1");
        map.put("key-input", "0");
        map.put("kode-input", UUID);
        map.put("proyek-input", kodeProyek);
        map.put("pegawai-input", kodeKaryawan);
        map.put("pemohon-input", sessionManager.retrieve(SessionManager.BIO));
        map.put("status-input", "ajuan");
        map.put("tanggal-input", Utility.getText(tanggalKlaimTextView));
        map.put("nominal-input", Utility.getText(nominalEditText));
        map.put("perihal-input", Utility.getText(perihalEditText));
        map.put("terpakai-input", "1");

        Call<SimpanResponse> service = api.simpan(map);

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        uploadLampiran(UUID);
//                        finish();
                    } else {
                        dismissProgressDialog();
                        toast("Klaim Gagal");
                    }
                } else {
                    dismissProgressDialog();
                    toast("Klaim Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Klaim Gagal");
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));
            } else if (requestCode == Constants.REQUEST_ID_KARYAWAN) {
                kodeKaryawan = data.getStringExtra("id");
                karyawanTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    void openCalendar() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(InputKlaimActivity.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String sMonth = String.valueOf(monthOfYear);
                        if (sMonth.length() == 1) {
                            sMonth = "0" + (Integer.parseInt(sMonth) + 1);
                            sMonth = sMonth.substring(sMonth.length() - 2);
                        }

                        String sDay = String.valueOf(dayOfMonth);
                        if (sDay.length() == 1) {
                            sDay = "0" + sDay;
                        }

                        tanggalKlaimTextView.setText(sDay + "/" + sMonth + "/" + year);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void uploadLampiran(String UUID) {
//        showProgressDialog("Upload Lampiran ...");
        File compressFile = Utility.compressFile(getApplicationContext(), lampiranFile);
        String gambar = Utility.imageToBase64(compressFile);
        Call<SimpanResponse> service = api.uploadGambar(UUID, gambar, "klaim");

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast("Klaim Berhasil");
                        finish();
                    } else {
                        toast("Upload Gagal");
                    }
                } else {
                    toast("Upload Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Upload Gagal");
            }
        });
    }

    @Override
    public void onFileSelected(File imageFile) {
        lampiranTextView.setText(imageFile.getName());
        lampiranFile = imageFile;
    }
}
