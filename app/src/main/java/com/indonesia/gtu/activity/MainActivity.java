package com.indonesia.gtu.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

import com.indonesia.gtu.R;
import com.indonesia.gtu.fragment.HomeFragment;
import com.indonesia.gtu.fragment.ProfileFragment;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

//        validateSession();

        /*FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(MainActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String newToken = instanceIdResult.getToken();
                updateToken(newToken);
            }
        });*/

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_home:
                                pushFragments(Constants.FRAGMENT_HOME, HomeFragment.newInstance());
                                break;
                            case R.id.action_profile:
                                pushFragments(Constants.FRAGMENT_PROFILE, ProfileFragment.newInstance());
                                break;
                        }

                        return true;
                    }
                });

        bottomNavigationView.setSelectedItemId(R.id.action_home);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            if (sessionManager.retrieve(SessionManager.CHECK_IN_DATE) != null) {
                if (!Utility.getDate().equalsIgnoreCase(sessionManager.retrieve(SessionManager.CHECK_IN_DATE))) {
                    sessionManager.logout();
                }
            }

            if (sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID).equalsIgnoreCase("-")) {
                startActivityIntent(InputPresensiKorlapActivity.class);
            }
        }
    }

    public void pushFragments(String tag, Fragment fragment) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        if (manager.findFragmentByTag(tag) == null) {
            ft.add(R.id.frame_layout, fragment, tag);
        }

        Fragment fragmentMyCustomer = manager.findFragmentByTag(Constants.FRAGMENT_HOME);
        Fragment fragmentProfile = manager.findFragmentByTag(Constants.FRAGMENT_PROFILE);

        // Hide all Fragment
        if (fragmentMyCustomer != null) {
            ft.hide(fragmentMyCustomer);
        }

        if (fragmentProfile != null) {
            ft.hide(fragmentProfile);
        }

        // Show  current Fragment
        if (tag.equalsIgnoreCase(Constants.FRAGMENT_HOME)) {
            if (fragmentMyCustomer != null) {
                ft.show(fragmentMyCustomer);
            }
        }

        if (tag.equalsIgnoreCase(Constants.FRAGMENT_PROFILE)) {
            if (fragmentProfile != null) {
                ft.show(fragmentProfile);
            }
        }
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        this.moveTaskToBack(true);
    }
}
