package com.indonesia.gtu.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.response.SimpanResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputPresensiKorlapActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    String kodeProyek;

    int getLocationAttemptCount = 1;
    int MAX_GET_LOCATION_ATTEMPT_COUNT = 5;
    private TextView addressCheckIn;
    Geocoder geocoder;
    List<Address> addresses;

    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    private GoogleMap mMap;

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.addressTextView)
    TextView addressTextView;

    @OnClick(R.id.checkInButton)
    public void checkInButton(Button button) {
        if (mLastLocation == null) {
            toast("Please turn on GPS");
            settingRequest();
        } else if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
            chooseProyek();
        } else {
            presensiProcess();
        }
    }

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        chooseProyek();
    }

    void chooseProyek() {
        Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_presensi_korlap);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Absensi Korlap");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        geocoder = new Geocoder(this, Locale.getDefault());

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else
            toast("Not Connected!");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.checkInMap);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
    }

    /*Ending the updates for the location service*/
    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        settingRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {
        toast("Connection Suspended!");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        toast("Connection Failed!");
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, 90000);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("Current Location", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    /*Method to get the enable location settings dialog*/
    @SuppressLint("RestrictedApi")
    public void settingRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {

            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can
                        // initialize location requests here.
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(InputPresensiKorlapActivity.this, Constants.REQUEST_ID_CHECK_SETTINGS_GPS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.
                        break;
                }
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case Constants.REQUEST_ID_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        getLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        toast("Location Service not Enabled");
                        break;
                    default:
                        break;
                }
                break;
            case Constants.REQUEST_ID_PROYEK:
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));
                break;
        }
    }

    public void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        } else {
            /*Getting the location after aquiring location service*/
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, InputPresensiKorlapActivity.this);

            if (mLastLocation != null) {
                setCurrentLocation();
            } else {
                /*if there is no last known location. Which means the device has no data for the loction currently.
                * So we will get the current location.
                * For this we'll implement Location Listener and override onLocationChanged*/
                Log.i("Current Location", "No data for location found");

                if (!mGoogleApiClient.isConnected())
                    mGoogleApiClient.connect();
            }

        }
    }

    void setCurrentLocation() {
        mMap.clear();
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), 1);
        } catch (IOException e) {
            getLocationAttemptCount++;
            if (getLocationAttemptCount == MAX_GET_LOCATION_ATTEMPT_COUNT) {
                toast("Failed to point the GPS");
            }
            e.printStackTrace();
        }

        if (addresses != null) {
            String completeAddress = "";
            int i = 0;
            for (Address address : addresses) {
                if (!TextUtils.isEmpty(address.toString())) {
                    completeAddress = addresses.get(i).getAddressLine(0)
                            + ", " + addresses.get(i).getLocality() + ", " + addresses.get(i).getCountryName();
                    break;
                }
                i++;
            }

            addressTextView.setText(completeAddress);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LatLng currentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(currentLocation)
                    .zoom(15)
                    .build();
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            mMap.addMarker(new MarkerOptions().position(currentLocation).title("Your Location"));
        }
    }

    /*When Location changes, this method get called. */
    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        setCurrentLocation();
    }

    private void presensiProcess() {
        showProgressDialog();
        /*
        model-input:gps
        action-input:1
        key-input:0
        kode-input:3c1f3699c741bf5cea7cc9f373jms215
        proyek-input:3c1f3699c741bf5cea7cc9f3736mi215
        biodata-input:3c1f3699c741bf5cea7cc9f3736mi215
        waktu-input:2019-06-18 17:42:08
        long-input:106.815175
        lat-input:-6.2308186
        terpakai-input:1
        */

        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "gps");
        map.put("action-input", "1");
        map.put("key-input", "0");
        map.put("kode-input", Utility.generateUUID());
        map.put("proyek-input", kodeProyek);
        map.put("biodata-input", sessionManager.retrieve(SessionManager.BIO));
        map.put("waktu-input", Utility.now());
        map.put("long-input", String.valueOf(mLastLocation.getLongitude()));
        map.put("lat-input", String.valueOf(mLastLocation.getLatitude()));
        map.put("terpakai-input", "1");

        Call<SimpanResponse> service = api.simpan(map);

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        sessionManager.putString(SessionManager.DEFAULT_PROYEK_ID, kodeProyek);
                        sessionManager.putString(SessionManager.DEFAULT_PROYEK_NAME, Utility.getText(proyekTextView));

                        sessionManager.putString(SessionManager.CHECK_IN_DATE, Utility.getDate());

                        toast("Absensi Berhasil");
                        finish();
                    } else {
                        toast("Absensi Gagal");
                    }
                } else {
                    toast("Absensi Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Absensi Gagal");
            }
        });
    }

}