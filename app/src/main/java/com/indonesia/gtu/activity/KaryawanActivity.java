package com.indonesia.gtu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.indonesia.gtu.R;
import com.indonesia.gtu.adapter.KaryawanAdapter;
import com.indonesia.gtu.fragment.ProyekFilterBottomSheetDialog;
import com.indonesia.gtu.helper.ClickListener;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.model.Karyawan;
import com.indonesia.gtu.response.KaryawanResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KaryawanActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener,
        ProyekFilterBottomSheetDialog.BottomSheetFilterListener {

    String requestType = "", proyekId = "", proyekText = "";

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.filterConstraintLayout)
    public void filterConstraintLayoutCLick(ConstraintLayout constraintLayout) {
        openFilterBottomSheet();
    }

    @BindView(R.id.filterConstraintLayout)
    ConstraintLayout filterConstraintLayout;

    SearchView searchView;
    List<Karyawan> karyawanList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_karyawan);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Data Karyawan");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.swipeRefreshLayout_1, R.color.swipeRefreshLayout_2, R.color.swipeRefreshLayout_3);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            filterConstraintLayout.setVisibility(View.GONE);
        } else {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (dy > 0 && filterConstraintLayout.getVisibility() == View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animHide);
                    } else if (dy < 0 && filterConstraintLayout.getVisibility() != View.VISIBLE) {
                        filterConstraintLayout.startAnimation(animShow);
                    }
                }
            });
        }

        if (getIntent().hasExtra(Constants.REQUEST_TYPE)) {
            requestType = getIntent().getStringExtra(Constants.REQUEST_TYPE);
        }

        if (getIntent().hasExtra("proyekId")) {
            proyekId = getIntent().getStringExtra("proyekId");
            proyekText = getIntent().getStringExtra("proyekText");
        }

        getKaryawanData();
    }

    @Override
    public void onRefresh() {
        searchView.setQuery("", false);
        searchView.requestFocus();
        getKaryawanData();
    }

    void getKaryawanData() {
        karyawanList = new ArrayList<>();
        swipeRefreshLayout.setRefreshing(true);
        recyclerView.setAdapter(null);

        String filterProyek = "a";

        if (proyekId.length() > 0) {
            filterProyek = proyekId;
        }

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KLIEN)
                || sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            filterProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
        }

        Call<KaryawanResponse> service = api.getKaryawanList(filterProyek);

        service.enqueue(new Callback<KaryawanResponse>() {
            @Override
            public void onResponse(Call<KaryawanResponse> call, Response<KaryawanResponse> response) {
                swipeRefreshLayout.setRefreshing(false);
                KaryawanResponse karyawanResponse = response.body();
                karyawanList = karyawanResponse.getKaryawanList();
                setAdapterDataRecycleView("");
            }

            @Override
            public void onFailure(Call<KaryawanResponse> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                call.cancel();
                toast(Constants.SERVER_RESPONSE_ERROR);
            }
        });
    }

    private void setAdapterDataRecycleView(String keyword) {
        final List<Karyawan> filterList = new ArrayList<>();

        if (keyword.length() > 0) {
            for (Karyawan karyawan : karyawanList) {
                if (karyawan.getNama().toLowerCase().contains(keyword)) {
                    filterList.add(karyawan);
                }
            }
        } else {
            filterList.addAll(karyawanList);
        }

        recyclerView.setAdapter(new KaryawanAdapter(filterList, getApplicationContext(), new ClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (requestType.equalsIgnoreCase(Constants.REQUEST_INTENT_RESULT)) {
                    Intent intent = new Intent();
//                    intent.putExtra("id", Utility.getCodeFromAksi(filterList.get(position).getAksi()));
                    intent.putExtra("id", filterList.get(position).getKode());
                    intent.putExtra("text", filterList.get(position).getNama());
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), KaryawanDetailActivity.class)
                            .putExtra("karyawanKode", filterList.get(position).getKode()));
                }

            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    void openFilterBottomSheet() {
        Bundle bundle = new Bundle();
        bundle.putString("proyekId", proyekId);
        bundle.putString("proyekText", proyekText);

        ProyekFilterBottomSheetDialog bottomSheet = new ProyekFilterBottomSheetDialog();
        bottomSheet.setArguments(bundle);
        bottomSheet.setListener(this);
        bottomSheet.show(getSupportFragmentManager(), "proyekFilterBottomSheet");
    }

    @Override
    public void onButtonFilterClicked(String proyekId, String proyekText) {
        this.proyekId = proyekId;
        this.proyekText = proyekText;
        getKaryawanData();
    }

    @Override
    public void onButtonrResetClicked() {
        this.proyekId = "";
        this.proyekText = "";
        getKaryawanData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                setAdapterDataRecycleView(newText);
                return true;
            }
        });

        return true;
    }
}
