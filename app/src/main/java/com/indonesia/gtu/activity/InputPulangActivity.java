package com.indonesia.gtu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.response.SimpanResponse;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InputPulangActivity extends BaseActivity {

    String kodeProyek, kodePresensi;

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.absensiTextView)
    TextView absensiTextView;

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        if (!sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            chooseProyek();
        }
    }

    void chooseProyek() {
        Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
    }

    @OnClick(R.id.absensiTextView)
    public void absensiTextViewClick(TextView textView) {
        if (Utility.isEmpty(proyekTextView)) {
            chooseProyek();
        } else {
            Intent intent = new Intent(getApplicationContext(), PresensiActivity.class)
                    .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT)
                    .putExtra("proyekId", kodeProyek)
                    .putExtra("proyekText", Utility.getText(proyekTextView));
            ;
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivityForResult(intent, Constants.REQUEST_ID_PRESENSI);
        }
    }

    @OnClick(R.id.simpanButton)
    public void simpanButton(Button button) {
        if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
        } else if (Utility.isEmpty(absensiTextView)) {
            toast("Karyawan tidak boleh kosong");
        } else {
            pulangProcess();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_pulang);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Input Pulang");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (sessionManager.retrieve(SessionManager.OTORITAS).equalsIgnoreCase(Constants.OTORITAS_KORLAP)) {
            kodeProyek = sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_ID);
            proyekTextView.setText(sessionManager.retrieve(SessionManager.DEFAULT_PROYEK_NAME));
        }
    }

    private void pulangProcess() {
        showProgressDialog();

        HashMap<String, String> map = new HashMap<>();
        map.put("model-input", "pulang");
        map.put("action-input", "1");
        map.put("key-input", "0");
        map.put("kode-input", Utility.generateUUID());
        map.put("terpakai-input", "1");
        map.put("presensi-input", kodePresensi);
        map.put("waktu-input", Utility.now());

        Call<SimpanResponse> service = api.simpan(map);

        service.enqueue(new Callback<SimpanResponse>() {
            @Override
            public void onResponse(Call<SimpanResponse> call, Response<SimpanResponse> response) {
                dismissProgressDialog();
                SimpanResponse simpanResponse = response.body();
                if (simpanResponse != null) {
                    if (simpanResponse.getStatus().equalsIgnoreCase("1")) {
                        toast("Pulang Berhasil");
                        finish();
                    } else {
                        toast("Abensi Pulang Gagal");
                    }
                } else {
                    toast("Abensi Pulang Gagal");
                }
            }

            @Override
            public void onFailure(Call<SimpanResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
                toast("Abensi Pulang Gagal");
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));
            } else if (requestCode == Constants.REQUEST_ID_PRESENSI) {
                kodePresensi = data.getStringExtra("id");
                absensiTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
