package com.indonesia.gtu.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.response.LiveLocationResponse;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LiveLocationActivity extends BaseActivity implements OnMapReadyCallback {

    String kodeProyek = "", kodeKaryawan = "";
    boolean isActivityOnForeground = false;

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.karyawanTextView)
    TextView karyawanTextView;

    @OnClick(R.id.lihatButton)
    public void lihatButtonClick(Button button) {
        if (Utility.isEmpty(proyekTextView)) {
            toast("Proyek tidak boleh kosong");
        } else if (Utility.isEmpty(karyawanTextView)) {
            toast("Karyawan tidak boleh kosong");
        } else {
            getLiveLocacation();
        }
    }

    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), ProyekActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
    }

    @OnClick(R.id.karyawanTextView)
    public void karyawanTextViewClick(TextView textView) {
        Intent intent = new Intent(getApplicationContext(), KaryawanActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_KARYAWAN);
    }

    private static final String TAG = LiveLocationActivity.class.getSimpleName();
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    private GoogleMap mMap;
    CameraPosition cameraPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_location);
        ButterKnife.bind(this);

        assert getSupportActionBar() != null;
        getSupportActionBar().setTitle("Live GPS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isActivityOnForeground) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            getLiveLocacation();
                        }
                    });
                }
            }
        }, 0, 15000);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                kodeProyek = data.getStringExtra("id");
                proyekTextView.setText(data.getStringExtra("text"));

                kodeKaryawan = "";
                karyawanTextView.setText("");
            } else if (requestCode == Constants.REQUEST_ID_KARYAWAN) {
                kodeKaryawan = data.getStringExtra("id");
                karyawanTextView.setText(data.getStringExtra("text"));
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }

    private void getLiveLocacation() {
        if (mMap == null) {
            toast("Please wait");
        } else if (!Utility.isEmpty(proyekTextView) && !Utility.isEmpty(karyawanTextView)) {
            refreshMap(mMap);

            Call<LiveLocationResponse> service = api.liveLocation(kodeProyek, kodeKaryawan);
            service.enqueue(new Callback<LiveLocationResponse>() {
                @Override
                public void onResponse(Call<LiveLocationResponse> call, Response<LiveLocationResponse> response) {

                    LiveLocationResponse liveLocationResponse = response.body();

                    if (liveLocationResponse != null) {
                        LiveLocationResponse.LiveLocation liveLocation = liveLocationResponse.getLiveLocation();

                        if (liveLocation.getLatitude().trim().equalsIgnoreCase("")) {
                            toast("Data tidak ditemukan");
                        } else {
                            LatLng latLng = new LatLng(Double.parseDouble(liveLocation.getLatitude()), Double.parseDouble(liveLocation.getLongitude()));

                            mMap.addMarker(new MarkerOptions().position(latLng).title(liveLocation.getWaktu()));
                            if (cameraPosition == null) {
                                CameraPosition cameraPosition = new CameraPosition.Builder()
                                        .target(latLng)
                                        .zoom(14)
                                        .build();
                                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            } else {
                                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }
                        }

                    } else {
                        toast(Constants.SERVER_RESPONSE_ERROR);
                    }
                }

                @Override
                public void onFailure(Call<LiveLocationResponse> call, Throwable t) {
                    dismissProgressDialog();
                    call.cancel();
                }
            });
        }
    }

    private void refreshMap(GoogleMap mapInstance) {
        mapInstance.clear();
    }

    @Override
    public void onStart() {
        super.onStart();
        isActivityOnForeground = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        isActivityOnForeground = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isActivityOnForeground = false;
    }
}
