package com.indonesia.gtu.model;

/**
 * Created by doni.wahyu on 11/4/2018.
 */

public class Menu {
    private String menuName;
    private int resourceId;
    private boolean isEnable;


    public Menu(String menuName, int resourceId, boolean isEnable) {
        this.menuName = menuName;
        this.resourceId = resourceId;
        this.isEnable = isEnable;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }
}
