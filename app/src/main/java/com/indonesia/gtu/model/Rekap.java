package com.indonesia.gtu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by doni.wahyu on 6/22/2019.
 */

public class Rekap implements Parcelable {
    private String kode;
    private String jenis;
    private String proyek;
    private String tanggal;
    private String waktu;
    private String status;
    private String tahap;

    public Rekap() {
    }

    protected Rekap(Parcel in) {
        kode = in.readString();
        jenis = in.readString();
        proyek = in.readString();
        tanggal = in.readString();
        waktu = in.readString();
        status = in.readString();
        tahap = in.readString();
    }

    public static final Creator<Rekap> CREATOR = new Creator<Rekap>() {
        @Override
        public Rekap createFromParcel(Parcel in) {
            return new Rekap(in);
        }

        @Override
        public Rekap[] newArray(int size) {
            return new Rekap[size];
        }
    };

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getProyek() {
        return proyek;
    }

    public void setProyek(String proyek) {
        this.proyek = proyek;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTahap() {
        return tahap;
    }

    public void setTahap(String tahap) {
        this.tahap = tahap;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(kode);
        parcel.writeString(jenis);
        parcel.writeString(proyek);
        parcel.writeString(tanggal);
        parcel.writeString(waktu);
        parcel.writeString(status);
        parcel.writeString(tahap);
    }
}
