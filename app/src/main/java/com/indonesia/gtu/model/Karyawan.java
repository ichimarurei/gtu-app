package com.indonesia.gtu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by doni.wahyu on 6/22/2019.
 */

public class Karyawan implements Parcelable {
    private String kode;
    private String id;
    private String nama;
    private String telepon;
    private String akun;
    private String proyek;
    private String jabatan;
    private String kontrak;
    private String aksi;

    public Karyawan() {
    }

    protected Karyawan(Parcel in) {
        kode = in.readString();
        id = in.readString();
        nama = in.readString();
        telepon = in.readString();
        akun = in.readString();
        proyek = in.readString();
        jabatan = in.readString();
        kontrak = in.readString();
        aksi = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(kode);
        dest.writeString(id);
        dest.writeString(nama);
        dest.writeString(telepon);
        dest.writeString(akun);
        dest.writeString(proyek);
        dest.writeString(jabatan);
        dest.writeString(kontrak);
        dest.writeString(aksi);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Karyawan> CREATOR = new Creator<Karyawan>() {
        @Override
        public Karyawan createFromParcel(Parcel in) {
            return new Karyawan(in);
        }

        @Override
        public Karyawan[] newArray(int size) {
            return new Karyawan[size];
        }
    };

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTelepon() {
        return telepon;
    }

    public void setTelepon(String telepon) {
        this.telepon = telepon;
    }

    public String getAkun() {
        return akun;
    }

    public void setAkun(String akun) {
        this.akun = akun;
    }

    public String getProyek() {
        return proyek;
    }

    public void setProyek(String proyek) {
        this.proyek = proyek;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getKontrak() {
        return kontrak;
    }

    public void setKontrak(String kontrak) {
        this.kontrak = kontrak;
    }

    public String getAksi() {
        return aksi;
    }

    public void setAksi(String aksi) {
        this.aksi = aksi;
    }
}
