package com.indonesia.gtu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by doni.wahyu on 6/22/2019.
 */

public class Proyek implements Parcelable{
    private String kode;
    private String proyek;
    private String tipe;
    private String lokasi;
    private String kendaraan;
    private String aksi;

    public Proyek() {
    }

    protected Proyek(Parcel in) {
        kode = in.readString();
        proyek = in.readString();
        tipe = in.readString();
        lokasi = in.readString();
        kendaraan = in.readString();
        aksi = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(kode);
        dest.writeString(proyek);
        dest.writeString(tipe);
        dest.writeString(lokasi);
        dest.writeString(kendaraan);
        dest.writeString(aksi);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Proyek> CREATOR = new Creator<Proyek>() {
        @Override
        public Proyek createFromParcel(Parcel in) {
            return new Proyek(in);
        }

        @Override
        public Proyek[] newArray(int size) {
            return new Proyek[size];
        }
    };

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getProyek() {
        return proyek;
    }

    public void setProyek(String proyek) {
        this.proyek = proyek;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getKendaraan() {
        return kendaraan;
    }

    public void setKendaraan(String kendaraan) {
        this.kendaraan = kendaraan;
    }

    public String getAksi() {
        return aksi;
    }

    public void setAksi(String aksi) {
        this.aksi = aksi;
    }
}
