package com.indonesia.gtu.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.activity.ProyekActivity;
import com.indonesia.gtu.helper.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by doni.wahyu on 6/27/2019.
 */

public class ProyekFilterBottomSheetDialog extends BottomSheetDialogFragment {
    private BottomSheetFilterListener mListener;
    String proyekId = "";
    String proyekText = "";

    @OnClick(R.id.applyButton)
    public void applyButtonClick(Button button) {
        mListener.onButtonFilterClicked(proyekId, proyekText);
        dismiss();
    }

    @OnClick(R.id.resetButton)
    public void resetButtonClick(Button button) {
        mListener.onButtonrResetClicked();
        dismiss();
    }

    public void setListener(BottomSheetFilterListener listener) {
        this.mListener = listener;
    }


    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    
    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        chooseProyek();
    }

    void chooseProyek() {
        Intent intent = new Intent(getContext(), ProyekActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
    }
 

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                proyekTextView.setText(data.getStringExtra("text"));
                proyekId = data.getStringExtra("id");
                proyekText = data.getStringExtra("text");
            } 
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_project_filter, container, false);
        ButterKnife.bind(this, v);

        proyekId = getArguments().getString("proyekId");
        proyekText = getArguments().getString("proyekText");
        proyekTextView.setText(proyekText);

        return v;
    }

    public interface BottomSheetFilterListener {
        void onButtonFilterClicked(String proyekId, String proyekText);

        void onButtonrResetClicked();
    }
}

