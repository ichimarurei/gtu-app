package com.indonesia.gtu.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.helper.Constants;
import com.indonesia.gtu.helper.SessionManager;
import com.indonesia.gtu.helper.Utility;
import com.indonesia.gtu.response.BiodataResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by doni.wahyu on 9/25/2018.
 */

public class ProfileFragment extends BaseFragment {

    @BindView(R.id.fullNameTextView)
    TextView fullNameTextView;

    @BindView(R.id.nikTextView)
    TextView nikTextView;

    @BindView(R.id.emailTextView)
    TextView emailTextView;

    @BindView(R.id.noHpTextView)
    TextView noHpTextView;

    @BindView(R.id.appVersionTextView)
    TextView appVersionTextView;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @OnClick(R.id.logoutButton)
    public void logoutButton(Button button) {
        logoutUser();
    }

    public void logoutUser() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle(R.string.app_name);
        alertDialogBuilder
                .setMessage("Anda yakin ingin keluar ?")
                .setCancelable(false)
                .setPositiveButton("Ya",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sessionManager.logout();
                            }
                        })

                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, null);
        ButterKnife.bind(this, rootView);

        biodata();

        return rootView;
    }

    private void biodata() {
        showProgressDialog();

        Call<BiodataResponse> service = api.biodata("bio", sessionManager.retrieve(SessionManager.BIO));

        service.enqueue(new Callback<BiodataResponse>() {
            @Override
            public void onResponse(Call<BiodataResponse> call, Response<BiodataResponse> response) {
                dismissProgressDialog();
                BiodataResponse biodataResponse = response.body();

                if (biodataResponse != null) {
                    String[] noTelp = biodataResponse.getBiodata().getTelepon().split("_");
                    String strTelp = "";

                    if (!noTelp[0].equals("-")) {
                        strTelp += noTelp[0];
                    }

                    if (!noTelp[1].equals("-")) {
                        strTelp += ", ".concat(noTelp[1]);
                    }

                    fullNameTextView.setText(biodataResponse.getBiodata().getNama());
                    nikTextView.setText(biodataResponse.getBiodata().getId());
                    emailTextView.setText(biodataResponse.getBiodata().getEmail());
                    noHpTextView.setText(strTelp);
                    appVersionTextView.setText(Utility.getAppVersionName(getContext()));
                } else {
                    toast(Constants.SERVER_RESPONSE_ERROR);
                }
            }

            @Override
            public void onFailure(Call<BiodataResponse> call, Throwable t) {
                dismissProgressDialog();
                call.cancel();
            }
        });
    }


}


