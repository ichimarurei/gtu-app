package com.indonesia.gtu.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.indonesia.gtu.R;
import com.indonesia.gtu.activity.ProyekActivity;
import com.indonesia.gtu.helper.Constants;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by doni.wahyu on 6/27/2019.
 */

public class PresensiFilterBottomSheetDialog extends BottomSheetDialogFragment {
    private BottomSheetFilterListener mListener;
    String proyekId = "", proyekText = "", startDate = "", endDate = "";

    @OnClick(R.id.applyButton)
    public void applyButtonClick(Button button) {
        mListener.onButtonFilterClicked(proyekId, proyekText, startDate, endDate);
        dismiss();
    }

    @OnClick(R.id.resetButton)
    public void resetButtonClick(Button button) {
        mListener.onButtonrResetClicked();
        dismiss();
    }

    @OnClick(R.id.startDateTextView)
    public void startDateTextViewClick(TextView textView) {
        openCalendar("start");
    }

    @OnClick(R.id.endDateTextView)
    public void endDateTextViewClick(TextView textView) {
        openCalendar("end");
    }

    public void setListener(BottomSheetFilterListener listener) {
        this.mListener = listener;
    }

    @BindView(R.id.proyekTextView)
    TextView proyekTextView;

    @BindView(R.id.startDateTextView)
    TextView startDateTextView;

    @BindView(R.id.endDateTextView)
    TextView endDateTextView;


    @OnClick(R.id.proyekTextView)
    public void proyekTextViewClick(TextView textView) {
        chooseProyek();
    }

    void chooseProyek() {
        Intent intent = new Intent(getContext(), ProyekActivity.class)
                .putExtra(Constants.REQUEST_TYPE, Constants.REQUEST_INTENT_RESULT);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivityForResult(intent, Constants.REQUEST_ID_PROYEK);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_ID_PROYEK) {
                proyekTextView.setText(data.getStringExtra("text"));
                proyekId = data.getStringExtra("id");
                proyekText = data.getStringExtra("text");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.bottom_sheet_presensi_filter, container, false);
        ButterKnife.bind(this, v);

        proyekId = getArguments().getString("proyekId");
        proyekText = getArguments().getString("proyekText");
        startDate = getArguments().getString("startDate");
        endDate = getArguments().getString("endDate");
        proyekTextView.setText(proyekText);
        startDateTextView.setText(startDate);
        endDateTextView.setText(endDate);

        return v;
    }

    public interface BottomSheetFilterListener {
        void onButtonFilterClicked(String proyekId, String proyekText, String startDate, String endDate);

        void onButtonrResetClicked();
    }

    void openCalendar(final String dateType) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String sMonth = String.valueOf(monthOfYear);
                        if (sMonth.length() == 1) {
                            sMonth = "0" + (Integer.parseInt(sMonth) + 1);
                            sMonth = sMonth.substring(sMonth.length() - 2);
                        }

                        String sDay = String.valueOf(dayOfMonth);
                        if (sDay.length() == 1) {
                            sDay = "0" + sDay;
                        }

                        String sDate = year + "-" + sMonth + "-" + sDay;
                        if (dateType.equalsIgnoreCase("start")) {
                            startDateTextView.setText(sDate);
                            startDate = sDate;
                        } else {
                            endDateTextView.setText(sDate);
                            endDate = sDate;
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }
}

