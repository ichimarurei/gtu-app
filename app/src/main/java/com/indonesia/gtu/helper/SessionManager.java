package com.indonesia.gtu.helper;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.indonesia.gtu.activity.SplashScreenActivity;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * Created by root on 14/11/16.
 */

public class SessionManager {
    // All Shared Preferences Keys

    public static final String AKUN = "akun";
    public static final String BIO = "bio";
    public static final String ID = "id";
    public static final String OTORITAS = "otoritas";
    public static final String DEFAULT_PROYEK_ID = "proyek_id";
    public static final String DEFAULT_PROYEK_NAME = "proyek_name";
    public static final String CHECK_IN_DATE = "check_in_date";

    private static final String VALID = "isValid";
    private SharedPreferences preferences; // Session Management
    private Editor editor;
    private Context context;

    public SessionManager(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences("gtu", 0); // using MODE_PRIVATE default operation mode
        editor = preferences.edit();
    }

    /**
     * Add session
     *
     * @param values
     */
    public void putString(HashMap<String, String> values) {
        Iterator iterator = values.entrySet().iterator();

        while (iterator.hasNext()) {
            Entry value = (Entry) iterator.next();
            editor.putString(value.getKey().toString(), value.getValue().toString());
            iterator.remove(); // avoids a ConcurrentModificationException
        }

        save();
    }

    public void putString(String key, String value) {
        editor.putString(key, value);
        save();
    }

    public String retrieve(String key) { // Get stored session data
        HashMap<String, String> sessionMap = (HashMap<String, String>) preferences.getAll();
        return sessionMap.get(key);
    }

    public void remove(String key) {
        editor.remove(key).apply();
    }

    /**
     * authorize sign in
     */
    public void authorize() {
        editor.putBoolean(VALID, true); // for sign in flag
        save();
    }

    public boolean isAuthorized() { // check for sign in flag
        return preferences.getBoolean(VALID, false);
    }

    public boolean isConnected() { // Determine Internet Connection
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public void redirect() { // force to sign in first
        Intent intent = new Intent(this.context, SplashScreenActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // Closing all the Activities
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // Add new Flag to start new Activity
        this.context.startActivity(intent);
    }

    public void clear() {
        // Clearing all data from Shared Preferences
        editor.clear();
        save();
    }

    public void logout() {
        clear();
        redirect();
    }

    private void save() {
        editor.commit(); // commit changes
    }
}
