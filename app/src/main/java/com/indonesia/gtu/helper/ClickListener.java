package com.indonesia.gtu.helper;

import android.view.View;

/**
 * Created by doni.wahyu on 9/27/2017.
 */

public interface ClickListener {
    void onItemClick(View view, int position);
    void onItemLongClick(View view, int position);
}
