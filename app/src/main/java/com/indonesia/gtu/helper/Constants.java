package com.indonesia.gtu.helper;

public class Constants {
    public static final int TOTAL_REQUEST_PER_PAGE = 25;
    public final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x1;
    public final static int REQUEST_ID_CHECK_SETTINGS_GPS = 0x2;
    public final static int REQUEST_ID_WRITE_STORAGE = 0x3;
    public final static int REQUEST_ID_READ_STORAGE = 0x4;
    public final static int REQUEST_ID_VIDEO = 0x5;
    public final static int REQUEST_ID_REGION = 0x6;
    public final static int REQUEST_ID_READ_WRITE_STORAGE = 0x7;
    public final static int REQUEST_ID_PROYEK = 0x8;
    public final static int REQUEST_ID_QR_CODE = 0x10;
    public final static int REQUEST_ID_KARYAWAN = 0x11;
    public final static int REQUEST_ID_PRESENSI = 0x12;
    public static final String API_URL = "https://gtu.indesc.co.id/restapi/";
    public static final int DRAWABLE_LEFT = 1;
    public static final int DRAWABLE_RIGHT = 2;
    public static final int DRAWABLE_TOP = 3;
    // public static int SPLASH_TIME_OUT = 100; // ms
    public static final int DRAWABLE_BOTTOM = 4;
    public static final String SERVER_RESPONSE_ERROR = "Server Response Error";
    public static final String FRAGMENT_HOME = "fragmentHome";
    public static final String FRAGMENT_PROFILE = "fragmentProfile";
    public static final String MENU_PROYEK = "Proyek";
    public static final String MENU_KARYAWAN = "Karyawan";
    public static final String MENU_QR_CODE = "ID Saya";
    public static final String MENU_PRESENSI = "Absensi";
    public static final String MENU_INPUT_PRESENSI = "Input Absensi";
    public static final String MENU_INPUT_PRESENSI_KORLAP = "Absensi Korlap";
    public static final String MENU_LEMBUR = "Lembur";
    public static final String MENU_INPUT_LEMBUR = "Input Lembur";
    public static final String MENU_CUTI = "Cuti";
    public static final String MENU_INPUT_CUTI = "Input Cuti";
    public static final String MENU_KLAIM = "Klaim";
    public static final String MENU_KASBON = "Kasbon";
    public static final String MENU_INPUT_KLAIM = "Input Klaim";
    public static final String MENU_INPUT_KASBON = "Input Kasbon";
    public static final String MENU_INPUT_SHIPMENT = "Input Shipment";
    public static final String MENU_SHIPMENT = "Shipment";
    public static final String MENU_INPUT_KACIL = "Input Pengeluaran";
    public static final String MENU_KACIL = "Kas Kecil";
    public static final String MENU_PULANG = "Pulang";
    public static final String MENU_INPUT_PULANG = "Input Pulang";
    public static final String MENU_LIVE_LOCATION = "Live GPS";
    public static final String MENU_SURAT_PERINGATAN = "Surat Peringatan";
    public static final String MENU_KARIR = "Karir";
    public static final String MENU_REKAP_ABSENSI = "Rekap Absensi";
    public static final String MENU_REKAP_LEMBUR = "Rekap Lembur";
    public static final String REQUEST_TYPE = "requestType";
    public static final String REQUEST_INTENT_RESULT = "requestIntentResult";
    public static final String SESSION_PEGAWAI = "requestIntentResult";
    public static final String OTORITAS_KLIEN = "klien";
    public static final String OTORITAS_KORLAP = "korlap";
    public static final String OTORITAS_ADMIN = "admin";
    public static final String OTORITAS_PEGAWAI = "pegawai";
    public static int LOGLEVEL = 1;
    public static boolean WARN = LOGLEVEL > 1;
    public static boolean DEBUG = LOGLEVEL > 0;
    public static int SPLASH_TIME_OUT = 100; // ms
    public static String DEFAULT_LOADING_MESSAGE = "Please Wait ...";
    public static String LOGIN_SUCCESS = "success";
    public static String LOGIN_FAILED = "failed";
    public static String UNAUTHORIZED_ACCOUNT = "Session anda telah habis / anda login di perangkat yang berbeda";


}
